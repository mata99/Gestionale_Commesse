﻿namespace Gestionale_Commesse
{
    partial class frmAggiungiDestinazione
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAggiungiDestinazione));
            this.lblmoneta = new System.Windows.Forms.Label();
            this.lbllingua = new System.Windows.Forms.Label();
            this.lblIndirizzo = new System.Windows.Forms.Label();
            this.lblnazione = new System.Windows.Forms.Label();
            this.btnnuovo = new System.Windows.Forms.Button();
            this.txtCitta = new System.Windows.Forms.TextBox();
            this.lblCittà = new System.Windows.Forms.Label();
            this.txtHotel = new System.Windows.Forms.TextBox();
            this.txtIndirizzoHotel = new System.Windows.Forms.TextBox();
            this.txtLingua = new System.Windows.Forms.TextBox();
            this.txtNazione = new System.Windows.Forms.TextBox();
            this.txtmoneta = new System.Windows.Forms.TextBox();
            this.txtIndirizzo = new System.Windows.Forms.TextBox();
            this.lblNomehotel = new System.Windows.Forms.Label();
            this.lblIndHotel = new System.Windows.Forms.Label();
            this.lblRistorante = new System.Windows.Forms.Label();
            this.lblNristorante = new System.Windows.Forms.Label();
            this.txtIndirizRistorante = new System.Windows.Forms.TextBox();
            this.txtNristorante = new System.Windows.Forms.TextBox();
            this.btnAggiungiRistoranti = new System.Windows.Forms.Button();
            this.btnAggiungiHotel = new System.Windows.Forms.Button();
            this.txtIdLuogo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblmoneta
            // 
            this.lblmoneta.AutoSize = true;
            this.lblmoneta.Location = new System.Drawing.Point(273, 9);
            this.lblmoneta.Name = "lblmoneta";
            this.lblmoneta.Size = new System.Drawing.Size(43, 13);
            this.lblmoneta.TabIndex = 2;
            this.lblmoneta.Text = "Moneta";
            // 
            // lbllingua
            // 
            this.lbllingua.AutoSize = true;
            this.lbllingua.Location = new System.Drawing.Point(46, 61);
            this.lbllingua.Name = "lbllingua";
            this.lbllingua.Size = new System.Drawing.Size(39, 13);
            this.lbllingua.TabIndex = 8;
            this.lbllingua.Text = "Lingua";
            // 
            // lblIndirizzo
            // 
            this.lblIndirizzo.AutoSize = true;
            this.lblIndirizzo.Location = new System.Drawing.Point(248, 35);
            this.lblIndirizzo.Name = "lblIndirizzo";
            this.lblIndirizzo.Size = new System.Drawing.Size(71, 13);
            this.lblIndirizzo.TabIndex = 6;
            this.lblIndirizzo.Text = "Indirizzo sede";
            // 
            // lblnazione
            // 
            this.lblnazione.AutoSize = true;
            this.lblnazione.Location = new System.Drawing.Point(39, 13);
            this.lblnazione.Name = "lblnazione";
            this.lblnazione.Size = new System.Drawing.Size(46, 13);
            this.lblnazione.TabIndex = 0;
            this.lblnazione.Text = "Nazione";
            // 
            // btnnuovo
            // 
            this.btnnuovo.Location = new System.Drawing.Point(454, 139);
            this.btnnuovo.Name = "btnnuovo";
            this.btnnuovo.Size = new System.Drawing.Size(73, 23);
            this.btnnuovo.TabIndex = 22;
            this.btnnuovo.Text = "Nuovo";
            this.btnnuovo.UseVisualStyleBackColor = true;
            this.btnnuovo.Click += new System.EventHandler(this.btnnuovo_Click);
            // 
            // txtCitta
            // 
            this.txtCitta.Location = new System.Drawing.Point(91, 32);
            this.txtCitta.Name = "txtCitta";
            this.txtCitta.Size = new System.Drawing.Size(123, 20);
            this.txtCitta.TabIndex = 5;
            this.txtCitta.TextChanged += new System.EventHandler(this.txtCitta_TextChanged);
            // 
            // lblCittà
            // 
            this.lblCittà.AutoSize = true;
            this.lblCittà.Location = new System.Drawing.Point(22, 35);
            this.lblCittà.Name = "lblCittà";
            this.lblCittà.Size = new System.Drawing.Size(63, 13);
            this.lblCittà.TabIndex = 4;
            this.lblCittà.Text = "Città/Paese";
            // 
            // txtHotel
            // 
            this.txtHotel.Location = new System.Drawing.Point(91, 84);
            this.txtHotel.Name = "txtHotel";
            this.txtHotel.Size = new System.Drawing.Size(123, 20);
            this.txtHotel.TabIndex = 13;
            this.txtHotel.TextChanged += new System.EventHandler(this.txtHotel_TextChanged);
            // 
            // txtIndirizzoHotel
            // 
            this.txtIndirizzoHotel.Location = new System.Drawing.Point(325, 84);
            this.txtIndirizzoHotel.Name = "txtIndirizzoHotel";
            this.txtIndirizzoHotel.Size = new System.Drawing.Size(123, 20);
            this.txtIndirizzoHotel.TabIndex = 15;
            this.txtIndirizzoHotel.TextChanged += new System.EventHandler(this.txtIndirizzoHotel_TextChanged);
            // 
            // txtLingua
            // 
            this.txtLingua.Location = new System.Drawing.Point(91, 58);
            this.txtLingua.Name = "txtLingua";
            this.txtLingua.Size = new System.Drawing.Size(123, 20);
            this.txtLingua.TabIndex = 9;
            this.txtLingua.TextChanged += new System.EventHandler(this.txtLingua_TextChanged);
            // 
            // txtNazione
            // 
            this.txtNazione.Location = new System.Drawing.Point(91, 6);
            this.txtNazione.Name = "txtNazione";
            this.txtNazione.Size = new System.Drawing.Size(123, 20);
            this.txtNazione.TabIndex = 1;
            this.txtNazione.TextChanged += new System.EventHandler(this.txtNazione_TextChanged);
            // 
            // txtmoneta
            // 
            this.txtmoneta.Location = new System.Drawing.Point(325, 6);
            this.txtmoneta.Name = "txtmoneta";
            this.txtmoneta.Size = new System.Drawing.Size(123, 20);
            this.txtmoneta.TabIndex = 3;
            this.txtmoneta.TextChanged += new System.EventHandler(this.txtmoneta_TextChanged);
            // 
            // txtIndirizzo
            // 
            this.txtIndirizzo.Location = new System.Drawing.Point(325, 32);
            this.txtIndirizzo.Name = "txtIndirizzo";
            this.txtIndirizzo.Size = new System.Drawing.Size(123, 20);
            this.txtIndirizzo.TabIndex = 7;
            this.txtIndirizzo.TextChanged += new System.EventHandler(this.txtIndirizzo_TextChanged);
            // 
            // lblNomehotel
            // 
            this.lblNomehotel.AutoSize = true;
            this.lblNomehotel.Location = new System.Drawing.Point(12, 91);
            this.lblNomehotel.Name = "lblNomehotel";
            this.lblNomehotel.Size = new System.Drawing.Size(73, 13);
            this.lblNomehotel.TabIndex = 12;
            this.lblNomehotel.Text = "Hotel trasferta";
            // 
            // lblIndHotel
            // 
            this.lblIndHotel.AutoSize = true;
            this.lblIndHotel.Location = new System.Drawing.Point(246, 84);
            this.lblIndHotel.Name = "lblIndHotel";
            this.lblIndHotel.Size = new System.Drawing.Size(73, 13);
            this.lblIndHotel.TabIndex = 14;
            this.lblIndHotel.Text = "Indirizzo Hotel";
            // 
            // lblRistorante
            // 
            this.lblRistorante.AutoSize = true;
            this.lblRistorante.Location = new System.Drawing.Point(228, 110);
            this.lblRistorante.Name = "lblRistorante";
            this.lblRistorante.Size = new System.Drawing.Size(91, 13);
            this.lblRistorante.TabIndex = 18;
            this.lblRistorante.Text = "Indirizzo ristorante";
            // 
            // lblNristorante
            // 
            this.lblNristorante.AutoSize = true;
            this.lblNristorante.Location = new System.Drawing.Point(30, 117);
            this.lblNristorante.Name = "lblNristorante";
            this.lblNristorante.Size = new System.Drawing.Size(55, 13);
            this.lblNristorante.TabIndex = 16;
            this.lblNristorante.Text = "Ristorante";
            // 
            // txtIndirizRistorante
            // 
            this.txtIndirizRistorante.Location = new System.Drawing.Point(325, 110);
            this.txtIndirizRistorante.Name = "txtIndirizRistorante";
            this.txtIndirizRistorante.Size = new System.Drawing.Size(123, 20);
            this.txtIndirizRistorante.TabIndex = 19;
            this.txtIndirizRistorante.TextChanged += new System.EventHandler(this.txtIndirizRistorante_TextChanged);
            // 
            // txtNristorante
            // 
            this.txtNristorante.Location = new System.Drawing.Point(91, 110);
            this.txtNristorante.Name = "txtNristorante";
            this.txtNristorante.Size = new System.Drawing.Size(123, 20);
            this.txtNristorante.TabIndex = 17;
            this.txtNristorante.TextChanged += new System.EventHandler(this.txtNristorante_TextChanged);
            // 
            // btnAggiungiRistoranti
            // 
            this.btnAggiungiRistoranti.Location = new System.Drawing.Point(454, 110);
            this.btnAggiungiRistoranti.Name = "btnAggiungiRistoranti";
            this.btnAggiungiRistoranti.Size = new System.Drawing.Size(75, 23);
            this.btnAggiungiRistoranti.TabIndex = 21;
            this.btnAggiungiRistoranti.Text = "Aggiungi ristorante";
            this.btnAggiungiRistoranti.UseVisualStyleBackColor = true;
            this.btnAggiungiRistoranti.Click += new System.EventHandler(this.btnAggiungi_Click);
            // 
            // btnAggiungiHotel
            // 
            this.btnAggiungiHotel.Location = new System.Drawing.Point(454, 82);
            this.btnAggiungiHotel.Name = "btnAggiungiHotel";
            this.btnAggiungiHotel.Size = new System.Drawing.Size(75, 23);
            this.btnAggiungiHotel.TabIndex = 20;
            this.btnAggiungiHotel.Text = "Aggiungi ristorante";
            this.btnAggiungiHotel.UseVisualStyleBackColor = true;
            this.btnAggiungiHotel.Click += new System.EventHandler(this.btnAggiungiHotel_Click);
            // 
            // txtIdLuogo
            // 
            this.txtIdLuogo.Location = new System.Drawing.Point(325, 58);
            this.txtIdLuogo.Name = "txtIdLuogo";
            this.txtIdLuogo.Size = new System.Drawing.Size(123, 20);
            this.txtIdLuogo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(270, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Id Luogo";
            // 
            // frmAggiungiDestinazione
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 170);
            this.Controls.Add(this.txtIdLuogo);
            this.Controls.Add(this.btnAggiungiHotel);
            this.Controls.Add(this.btnAggiungiRistoranti);
            this.Controls.Add(this.lblRistorante);
            this.Controls.Add(this.lblNristorante);
            this.Controls.Add(this.txtIndirizRistorante);
            this.Controls.Add(this.txtNristorante);
            this.Controls.Add(this.lblIndHotel);
            this.Controls.Add(this.lblNomehotel);
            this.Controls.Add(this.txtIndirizzoHotel);
            this.Controls.Add(this.txtHotel);
            this.Controls.Add(this.txtLingua);
            this.Controls.Add(this.lblCittà);
            this.Controls.Add(this.txtIndirizzo);
            this.Controls.Add(this.txtCitta);
            this.Controls.Add(this.txtNazione);
            this.Controls.Add(this.btnnuovo);
            this.Controls.Add(this.txtmoneta);
            this.Controls.Add(this.lblnazione);
            this.Controls.Add(this.lblIndirizzo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbllingua);
            this.Controls.Add(this.lblmoneta);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmAggiungiDestinazione";
            this.Text = "Aggiungi una destinazione";
            this.Load += new System.EventHandler(this.frmAggiungiDestinazione_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblmoneta;
        private System.Windows.Forms.Label lbllingua;
        private System.Windows.Forms.Label lblIndirizzo;
        private System.Windows.Forms.Label lblnazione;
        private System.Windows.Forms.Button btnnuovo;
        private System.Windows.Forms.TextBox txtCitta;
        private System.Windows.Forms.Label lblCittà;
        private System.Windows.Forms.TextBox txtHotel;
        private System.Windows.Forms.TextBox txtIndirizzoHotel;
        private System.Windows.Forms.TextBox txtLingua;
        private System.Windows.Forms.TextBox txtNazione;
        private System.Windows.Forms.TextBox txtmoneta;
        private System.Windows.Forms.TextBox txtIndirizzo;
        private System.Windows.Forms.Label lblNomehotel;
        private System.Windows.Forms.Label lblIndHotel;
        private System.Windows.Forms.Label lblRistorante;
        private System.Windows.Forms.Label lblNristorante;
        private System.Windows.Forms.TextBox txtIndirizRistorante;
        private System.Windows.Forms.TextBox txtNristorante;
        private System.Windows.Forms.Button btnAggiungiRistoranti;
        private System.Windows.Forms.Button btnAggiungiHotel;
        private System.Windows.Forms.TextBox txtIdLuogo;
        private System.Windows.Forms.Label label1;
    }
}