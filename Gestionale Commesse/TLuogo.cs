﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace Gestionale_Commesse
{
    /// <summary>
    /// Classe contenente tutti i metodi per la gestione della tabella luogo
    /// </summary>
    static class TLuogo
    {
        static string connString = Properties.Settings.ConnectionString;

        /// <summary>
        /// Funzione che salva un Luogo nell'omonima tabella
        /// </summary>
        public static bool Save(Luogo l)
        {

            string cmdString = "INSERT INTO Luogo (IDLuogo,Moneta,Lingua,Indirizzo,Nazione,Citta) " + "VALUES (@IDLuogo,@Moneta,@Lingua,@Indirizzo,@Nazione,@Citta)";
            using (SqlConnection conn = new SqlConnection(connString))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                cmd.Parameters.AddWithValue("@IDLuogo", l.IDLuogo);
                cmd.Parameters.AddWithValue("@Moneta", l.Moneta);
                cmd.Parameters.AddWithValue("@Lingua", l.Lingua);
                cmd.Parameters.AddWithValue("@Indirizzo", l.Indirizzo);
                cmd.Parameters.AddWithValue("@Nazione", l.Nazione);
                cmd.Parameters.AddWithValue("@Citta", l.Citta);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                return true;
            }
        }

        /// <summary>
        /// Funzione che aggiorna la tabella Luogo aggiungendone dei nuovi o sovrascrivendone alcuni esistenti, come parametri richiede una lista di Luoghi
        /// </summary>
        public static void Update(List<Luogo> lc)
        {
            string cmdString = "IF EXISTS (SELECT * FROM Luogo WHERE IDLuogo=@IDLuogo)" +
                " UPDATE Luogo SET Moneta = @Moneta, Lingua = @Lingua, Indirizzo = @Indirizzo, Nazione = @Nazione, Citta = @Citta" +
                " WHERE IDLuogo = @IDLuogo ELSE INSERT INTO Luogo (Moneta, Lingua, Indirizzo, Nazione, Citta, IDLuogo) VALUES (@Moneta, @Lingua, @Indirizzo, @Nazione, @Citta, @IDLuogo)";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                foreach (var c in lc)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = cmdString;
                    cmd.Parameters.AddWithValue("@Moneta", c.Moneta);
                    cmd.Parameters.AddWithValue("@Lingua", c.Lingua);
                    cmd.Parameters.AddWithValue("@Indirizzo", c.Indirizzo);
                    cmd.Parameters.AddWithValue("@Nazione", c.Nazione);
                    cmd.Parameters.AddWithValue("@Citta", c.Citta);
                    cmd.Parameters.AddWithValue("@IDLuogo", c.IDLuogo);
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        /// <summary>
        /// Funzione che conta il numero delle righe della tabella, ritorna -1 in caso si verifichino degli errori
        /// </summary>
        public static int RowCount()
        {
            try
            {
                string cmdString = "SELECT COUNT(*) FROM Luogo";
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = cmdString;
                    int n = (int)cmd.ExecuteScalar();
                    conn.Close();
                    return n;
                }
            }
            catch
            {
                return -1;
            }
        }

        /// <summary>
        /// Funzione che legge tutte le righe della tabella Luogo e restituisce i valori tramite una lista di tipo Luogo
        /// </summary>
        public static List<Luogo> Read()

        {
            List<Luogo> l = new List<Luogo>();
            string cmdString = "SELECT * FROM Luogo";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(new Luogo(reader["IDLuogo"].ToString(), reader["Moneta"].ToString(), reader["Lingua"].ToString(), reader["Indirizzo"].ToString(), reader["Nazione"].ToString(), reader["Citta"].ToString()));
                conn.Close();
                return l;
            }
        }

        /// <summary>
        /// Funzione che legge dalla tabella Luogo tutti gli IDLuogo e restituisce i valori tramite una lista di stringhe
        /// </summary>
        public static List<string> Destinazione()
        {
            List<string> l = new List<string>();
            string cmdString = "SELECT IDLuogo FROM Luogo";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(reader["IDLuogo"].ToString());
                conn.Close();
                return l;
            }
        }

        /// <summary>
        /// Funzione che cerca nella tabella Luogo un determinato luogo identificato da un ID e restituisce l'oggetto all'interno di una lista
        /// </summary>
        public static List<Luogo> Cerca(string IDLuogo)
        {
            List<Luogo> l = new List<Luogo>();
            string cmdString = "SELECT * FROM Luogo WHERE IDLuogo = @IDLuogo";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.AddWithValue("@IDLuogo", IDLuogo);
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(new Luogo(reader["IDLuogo"].ToString(), reader["Moneta"].ToString(), reader["Lingua"].ToString(), reader["Indirizzo"].ToString(), reader["Nazione"].ToString(), reader["Citta"].ToString()));
                conn.Close();
                return l;
            }
        }

        /// <summary>
        /// Funzione che ritorna tutti i luoghi dove ci sono state delle commesse
        /// </summary>
        public static List<Luogo> Commesse()
        {
            List<Luogo> l = new List<Luogo>();

            string cmdString = "SELECT * FROM Luogo,Commessa WHERE Commessa.IDLuogo=Luogo.IDLuogo";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                cmd.ExecuteNonQuery();

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    l.Add(new Luogo(reader["IDLuogo"].ToString(), reader["Moneta"].ToString(), reader["Lingua"].ToString(), reader["Indirizzo"].ToString(), reader["Nazione"].ToString(), reader["Citta"].ToString()));
                reader.Close();
                conn.Close();
            }
            return l;
        }

        public static bool Empty
        { get { return RowCount() <= 0; } }
    }
}