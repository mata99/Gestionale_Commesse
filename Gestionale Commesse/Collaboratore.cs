﻿using System;

namespace Gestionale_Commesse
{
    /// <summary>
    /// Classe per la gestione dei dati del collaboratore.
    /// </summary>
    class Collaboratore : IEquatable<Collaboratore>
    {
        public string Nome { get; private set; }
        public string Cognome { get; private set; }
        public string Reparto { get; private set; }
        public string Email { get; private set; }
        public string TelInterno { get; private set; }
        public string Cellulare { get; private set; }
        public string TipoDiCollaborazione { get; set; }
        public string DataAssunzione { get; private set; }
        public string Comune { get; private set; }
        public string IDCollaboratore { get; private set; }


        /// <summary>
        /// Costruttore della classe Collaboratore: non richiede l'IDCollaboratore
        /// </summary>
        public Collaboratore(string Nome, string Cognome, string Reparto, string Email, string TelInterno, string Cellulare, string TipoDiCollaborazione, string DataAssunzione, string Comune)
        {
            this.Nome = Nome;
            this.Cognome = Cognome;
            this.Reparto = Reparto;
            this.Email = Email;
            this.TelInterno = TelInterno;
            this.Cellulare = Cellulare;
            this.TipoDiCollaborazione = TipoDiCollaborazione;
            this.DataAssunzione = DataAssunzione;
            this.Comune = Comune;
            IDCollaboratore = CalcolaID();
        }

        /// <summary>
        /// Costruttore della classe Collaboratore: richiede l'IDCollaboratore
        /// </summary>
        public Collaboratore(string Nome, string Cognome, string Reparto, string Email, string TelInterno, string Cellulare, string TipoDiCollaborazione, string DataAssunzione, string Comune, string IDCollaboratore)
        {
            this.Nome = Nome;
            this.Cognome = Cognome;
            this.Reparto = Reparto;
            this.Email = Email;
            this.TelInterno = TelInterno;
            this.Cellulare = Cellulare;
            this.TipoDiCollaborazione = TipoDiCollaborazione;
            this.DataAssunzione = DataAssunzione;
            this.Comune = Comune;
            this.IDCollaboratore = IDCollaboratore;
        }

        /// <summary>
        /// Costruttore della classe Collaboratore
        /// Come parametri richiede un array di stringhe
        /// </summary>
        public Collaboratore(string[] parametri)
        {
            Cognome = parametri[0];
            Nome = parametri[1];
            Reparto = parametri[2];
            Email = parametri[3];
            TelInterno = parametri[4];
            Cellulare = parametri[5];
            TipoDiCollaborazione = parametri[6];
            DataAssunzione = parametri[7];
            Comune = parametri[8];
            IDCollaboratore = parametri[9];
        }

        /// <summary>
        /// Metodo che calcola l'ID del collaboratore sotto forma di stringa: prima lettera del nome + . + cognome + prime due cifre dell'anno di assunzione (se assunto)
        /// </summary>
        string CalcolaID()
        {
            DateTime dt;
            return Nome.ToLower()[0] + "." + Cognome.ToLower() + (DateTime.TryParse(DataAssunzione, out dt) ? dt.ToString("yy") : "");
        }

        /// <summary>
        /// Metodo che confronta il valore dell'oggetto con uno passatogli per parametro
        /// Ritorna true se gli oggetti sono uguali altrimenti ritorna false
        /// </summary>
        public bool EqualsAll(Collaboratore other)
        {
            if (Nome == other.Nome && Cognome == other.Cognome && Reparto == other.Reparto && Email == other.Email && TelInterno == other.TelInterno && Cellulare == other.Cellulare && TipoDiCollaborazione == other.TipoDiCollaborazione && DataAssunzione == other.DataAssunzione && Comune == other.Comune && IDCollaboratore == other.IDCollaboratore)
                return true;
            return false;
        }

        /// <summary>
        /// Metodo che confronta l'ID del collaboratore con un passatogli per parametro
        /// Restituisce true se sono uguali altrimenti false
        /// </summary>
        public bool Equals(Collaboratore other)
        {
            if (IDCollaboratore == other.IDCollaboratore)
                return true;
            return false;
        }
    }
}
