﻿namespace Gestionale_Commesse
{
    /// <summary>
    /// Classe per la gestione dei dati degli hotel.
    /// </summary>
    public class VittoAlloggio
    {
        public string IDVittoAlloggio { get; set; }
        public string IDLuogo { get; set; }
        public string Nome { get; private set; }
        public string Indirizzo { get; private set; }

        /// <summary>
        /// Costruttore della classe Hotel: richiede l'ID del luogo
        /// </summary>
        public VittoAlloggio(string IDLuogo, string Nome, string Indirizzo)
        {
            this.IDLuogo = IDLuogo;
            this.Nome = Nome;
            this.Indirizzo = Indirizzo;
            IDVittoAlloggio = CalcolaID();
        }

        /// <summary>
        /// Costruttore della classe Hotel: richiede l'ID dell'hotel e del luogo
        /// </summary>
        public VittoAlloggio(string IDVittoAlloggio, string IDLuogo, string Nome, string Indirizzo)
        {
            this.IDVittoAlloggio = IDVittoAlloggio;
            this.IDLuogo = IDLuogo;
            this.Nome = Nome;
            this.Indirizzo = Indirizzo;
        }

        /// <summary>
        /// Costruttore della classe Hotel: non richiede l'ID dell'hotel
        /// </summary>
        public VittoAlloggio(string Nome, string Indirizzo)
        {
            this.Nome = Nome;
            this.Indirizzo = Indirizzo;
            IDVittoAlloggio = CalcolaID();
        }

        /// <summary>
        /// Metodo che calcola l'ID dell'hotel sotto forma di stringa: nome + indirizzo
        /// </summary>
        string CalcolaID()
        {
            return string.Format("{0}{1}", Nome, Indirizzo);
        }
    }
}
