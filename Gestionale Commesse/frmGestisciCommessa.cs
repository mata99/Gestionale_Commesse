﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Gestionale_Commesse
{
    public partial class frmGestisciCommessa : Form
    {
        List<Collaboratore> lc = new List<Collaboratore>(); // Collaboratori prima di essere salvati in una commessa
        string IdLuogo;
        public frmGestisciCommessa()
        {
            InitializeComponent();
            btnNuova.Enabled = false;
        }

        private void frmAggiungiCommessa_Load(object sender, EventArgs e)
        {
            VisualizzaPannelli(false, panel1, panel2, panel3);
            VisualizzaPannelli(true, panel4);
            CaricaCombo();
            btnEliminaCommessa.Enabled = false;
        }

        /// <summary>
        /// Funzione che pulisce e ricarica le combobox della form in base al contenuto dei database
        /// </summary>
        private void CaricaCombo()
        {
            try
            {
                cmbCodiceCommessa.SelectedIndexChanged -= new EventHandler(cmbCodiceCommessa_SelectedIndexChanged);
                cmbCodiceCommessa.Items.Clear();
                cmbcollaboratore.Items.Clear();
                cmbDestinazione.Items.Clear();

                // Riempo il combobox con i nominativi dei collaboratori
                cmbcollaboratore.Items.AddRange(TCollaboratore.Nominativi().ToArray());
                cmbcollaboratore.AutoCompleteSource = AutoCompleteSource.ListItems;
                cmbcollaboratore.AutoCompleteMode = AutoCompleteMode.SuggestAppend;


                // Riempo il combobox gli ID dei luoghi
                cmbDestinazione.Items.AddRange(TLuogo.Destinazione().ToArray());
                cmbDestinazione.AutoCompleteSource = AutoCompleteSource.ListItems;
                cmbDestinazione.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

                // Riempo il combobox gli ID delle commesse
                cmbCodiceCommessa.Items.AddRange(TCommessa.IDCommessa().ToArray());
                cmbCodiceCommessa.AutoCompleteSource = AutoCompleteSource.ListItems;
                cmbCodiceCommessa.AutoCompleteMode = AutoCompleteMode.SuggestAppend;


                cmbCodiceCommessa.SelectedIndexChanged += new EventHandler(cmbCodiceCommessa_SelectedIndexChanged);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnChiudi_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Richiamo la form GestisciDatiLuoghi per visualizzare i dettagli di una destinazione di una commessa
        /// </summary>
        private void btnDettagli_Click(object sender, EventArgs e)
        {
            AggiornaDGV(TLuogo.Cerca(cmbDestinazione.Text));
            VisualizzaPannelli(true, panel1);
            VisualizzaPannelli(false, panel2, panel3, panel4);
        }

        /// <summary>
        /// Aggiorno la datagridview con il collaboratore selezionato
        /// </summary>
        private void cmbcollaboratore_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbcollaboratore.SelectedIndex != -1) // Combox vuoto?
                try
                {
                    Collaboratore c = TCollaboratore.Cerca_Nominativo(cmbcollaboratore.Text)[0];
                    if (!Exist(c))
                    {
                        dgwcollaboratori.Rows.Add(Row(c, 0));

                        // Aggiorno la DGV
                        foreach (DataGridViewRow item in dgwcollaboratori.Rows)
                        {
                            c = TCollaboratore.Cerca_Nominativo(item.Cells[0].Value.ToString() + " " + item.Cells[1].Value.ToString())[0]; // Estraggo un collaboratore
                            if (!lc.Contains(c)) // Verifico che il collaboratore non sia presente nella lista
                            {
                                lc.Add(c);
                                dgwcollaboratori.Rows[item.Index].SetValues(Row(c, 1));
                                dgwcollaboratori.Rows[item.Index].DefaultCellStyle.BackColor = Color.GreenYellow;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
        }

        private void btnNuova_Click(object sender, EventArgs e)
        {
            try
            {
                TPartecipa.Update(IdCollaboratori(), IdNonCl(), cmbCodiceCommessa.Text); // Salvo sulla tabella
                TCommessa.Update(new Commessa(cmbCodiceCommessa.Text, txtcliente.Text, dtpdata.Value, cmbDestinazione.Text)); // Salvo sulla tabella
                if (btnNuova.Text == "Nuova commessa")
                { Pulisci(); cmbCodiceCommessa.Text = ""; CaricaCombo(); }
                else
                {
                    // Aggiorno il controllo con i nuovi dati
                    dgwcollaboratori.Rows.Clear();
                    foreach (var item in lc)
                        dgwcollaboratori.Rows.Add(Row(item, 0));

                    MessageBox.Show("Database aggiornato!", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                txtcliente.BackColor = Color.White;
                cmbDestinazione.BackColor = Color.White;
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
        }

        /// <summary>
        /// Funzione che ottiene una lista di id dei collaboratori da eliminare in base al colore della riga della DGV
        /// </summary>
        private List<string> IdNonCl()
        {
            List<string> l = new List<string>();

            foreach (DataGridViewRow item in dgwcollaboratori.Rows)
                if (item.DefaultCellStyle.BackColor == Color.LightCoral)
                    l.Add(item.Cells[4].Value.ToString());
            return l;
        }

        private void Pulisci()
        {
            txtcliente.Clear();
            cmbCodiceCommessa.SelectedIndex = -1;
            cmbDestinazione.SelectedIndex = -1;
            cmbcollaboratore.SelectedIndex = -1;
            dtpdata.Value = DateTime.Today;
            dgwcollaboratori.Rows.Clear();
            lc.Clear();
        }

        /// <summary>
        /// Funzione che estrapola dalla lista di collaboratori i loro ID, non vuole nessun parametro, ritorna una lista di stringhe
        /// </summary>
        private List<string> IdCollaboratori()
        {
            List<string> l = new List<string>();
            foreach (var item in lc)
                l.Add(item.IDCollaboratore);
            return l;
        }

        /// <summary>
        /// Funzione che restituisce un array di stringhe, come parametri vuole una variabile di tipo collboratore e una variaible di tipo intero che indica se devo 
        /// incrementare o meno il numero di commesse in cui è impegnato il collaboratore
        /// </summary>
        private string[] Row(Collaboratore c, int N)
        {
            int nCommesse = TPartecipa.Commesse_Collaboratore(c.IDCollaboratore) + N;
            return new string[] { c.Cognome, c.Nome, c.Reparto, nCommesse + " commess" + ((nCommesse == 1) ? 'a' : 'e'), c.IDCollaboratore };
        }

        /// <summary>
        /// Funzione che verifica se se un collaboratore è presente nella DataGridView, come parametri vuole una variabile di tipo collaboratore
        /// ritorna true se il collaboratore è presente, false se non è presente
        /// </summary>
        private bool Exist(Collaboratore c1)
        {
            foreach (DataGridViewRow item in dgwcollaboratori.Rows)
            {
                List<Collaboratore> lc = TCollaboratore.Cerca_Nominativo(item.Cells[0].Value.ToString() + " " + item.Cells[1].Value.ToString());
                Collaboratore c2 = lc[0];
                if (c1.EqualsAll(c2))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Funzione che aggiunge (se non già fatto) un collaboratore alla lista dei collaboratori che partecipano alla creazione di una commessa
        /// </summary>
        private void btnAggiungi_Click(object sender, EventArgs e)
        {

            try
            {
                foreach (DataGridViewRow item in dgwcollaboratori.Rows)
                {
                    Collaboratore c = TCollaboratore.Cerca_Nominativo(item.Cells[0].Value.ToString() + " " + item.Cells[1].Value.ToString())[0]; // Estraggo un collaboratore
                    if (!lc.Contains(c)) // Verifico che il collaboratore non sia presente nella lista
                    {
                        lc.Add(c);
                        dgwcollaboratori.Rows[item.Index].SetValues(Row(c, 1));
                        dgwcollaboratori.Rows[item.Index].DefaultCellStyle.BackColor = Color.GreenYellow;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnElimCollab_Click(object sender, EventArgs e)
        {
            if (dgwcollaboratori.SelectedCells.Count > 0)
                try
                {
                    int index = dgwcollaboratori.SelectedCells[0].RowIndex;
                    lc.Remove(TCollaboratore.Cerca_Nominativo(dgwcollaboratori[0, index].Value.ToString() + " " + dgwcollaboratori[1, index].Value.ToString())[0]); // Rimuovo un collaboratore dalla tabella
                    dgwcollaboratori.Rows[index].DefaultCellStyle.BackColor = Color.LightCoral;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
        }

        /// <summary>
        /// In base al testo presente sulla combobox viene settato il bottone modifica commessa o nuova commessa
        /// </summary>
        private void cmbCodiceCommessa_SelectedIndexChanged(object sender, EventArgs e)
        {
            VisualizzaPannelli(false, panel1, panel2, panel3);
            VisualizzaPannelli(true, panel4);
            try
            {
                dgwcollaboratori.Rows.Clear();
                lc.Clear();
                btnNuova.Text = "Modifica commessa";

                List<Collaboratore> l = TPartecipa.PartecipaCommessa(cmbCodiceCommessa.Text); // Valorizzo la lista con i collaboratori che partecipano ad una commessa
                foreach (var item in l)
                {
                    dgwcollaboratori.Rows.Add(Row(item, 0));
                    lc.Add(item);
                }

                Commessa c = TCommessa.Read(cmbCodiceCommessa.Text)[0]; // Estraggo i dettagli della commessa selezionata

                dtpdata.Value = c.Data;
                txtcliente.Text = c.Cliente;
                cmbDestinazione.Text = c.IDLuogo;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            txtcliente.BackColor = Color.White;
            cmbDestinazione.BackColor = Color.White;
        }

        /// <summary>
        /// Richiamo la routine per vedere le commesse per le quali il collaboratore è impegnato
        /// </summary>
        private void dgwcollaboratori_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                var SenderGrid = (DataGridView)sender;
                if (SenderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0 && dgwcollaboratori[e.ColumnIndex, e.RowIndex].Value.ToString() != "0 commesse")
                {
                    List<Commessa> l = TPartecipa.Dettagli_Commessa(dgwcollaboratori[4, e.RowIndex].Value.ToString());

                    List<Collaboratore> li = TCollaboratore.CercaID(dgwcollaboratori[e.ColumnIndex + 1, e.RowIndex].Value.ToString());

                    if (li.Count > 0 && lc.Contains(li[0]) && !TPartecipa.PartecipaCommessa(cmbCodiceCommessa.Text).Contains(li[0]))
                        l.Add(CommessaPerDettagli());
                    DettagliCollaboratori(l);
                    VisualizzaPannelli(true, panel3);
                    VisualizzaPannelli(false, panel1, panel2);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        private void DettagliCollaboratori(List<Commessa> l)
        {
            dataGridView1.Rows.Clear();
            foreach (var item in l)
                dataGridView1.Rows.Add(item.IDCommessa, item.Cliente, item.Data.ToShortDateString(), item.IDLuogo);
        }

        /// <summary>
        /// Ottiene un oggetto commessa temporaneo in base al contenuto degli oggetti della form
        /// </summary>
        private Commessa CommessaPerDettagli()
        {
            string ID = (string.IsNullOrWhiteSpace(cmbCodiceCommessa.Text)) ? "Non inserito" : cmbCodiceCommessa.Text;
            string Cliente = (string.IsNullOrWhiteSpace(txtcliente.Text)) ? "Non inserito" : txtcliente.Text;
            DateTime dt = (dtpdata.Value <= DateTime.Today) ? DateTime.Today.AddDays(1) : dtpdata.Value;
            string IDLuogo = (string.IsNullOrWhiteSpace(cmbDestinazione.Text)) ? "Non inserito" : cmbDestinazione.Text;
            return new Commessa(ID, Cliente, dt, IDLuogo);
        }

        private void txtcliente_TextChanged(object sender, EventArgs e)
        {
            Abilita();
            txtcliente.BackColor = Color.GreenYellow;
        }

        private void Abilita()
        {
            if (txtcliente.Text == "" || cmbCodiceCommessa.Text == "" || cmbDestinazione.Text == "" || lc.Count == 0)
                btnNuova.Enabled = false;
            else
                btnNuova.Enabled = true;
        }

        private void cmbCodiceCommessa_TextChanged(object sender, EventArgs e)
        {
            if (!cmbCodiceCommessa.Items.Contains(cmbCodiceCommessa.Text))
            {
                btnNuova.Text = "Nuova commessa";
                Pulisci();
                Abilita();
                txtcliente.BackColor = Color.White;
                cmbDestinazione.BackColor = Color.White;
            }
            else
                btnEliminaCommessa.Enabled = true;
        }

        private void cmbDestinazione_TextChanged(object sender, EventArgs e)
        {
            Abilita();
            cmbDestinazione.BackColor = Color.GreenYellow;
        }

        private void dgwcollaboratori_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            Abilita();
        }

        private void dgwcollaboratori_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            Abilita();
        }

        private void btnEliminaCommessa_Click(object sender, EventArgs e)
        {
            try
            {
                TCommessa.Delete(cmbCodiceCommessa.Text);
                cmbCodiceCommessa.SelectedIndexChanged -= new EventHandler(cmbCodiceCommessa_SelectedIndexChanged);
                Pulisci();
                cmbCodiceCommessa.SelectedIndexChanged += new EventHandler(cmbCodiceCommessa_SelectedIndexChanged);
                cmbCodiceCommessa.Text = "";
                CaricaCombo();
                btnEliminaCommessa.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgwcollaboratori_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Abilita();
        }

        private void btnAggiungiDestinazione_Click(object sender, EventArgs e)
        {
            new frmAggiungiDestinazione().Show();
        }

        private void panel1_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                cmbluogo.Items.Clear();
                cmbluogo.Items.AddRange(TLuogo.Destinazione().ToArray());
                cmbluogo.AutoCompleteSource = AutoCompleteSource.ListItems;
                cmbluogo.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            dgwluogo.CellValueChanged += new DataGridViewCellEventHandler(dgwluogo_CellValueChanged_1);
        }



        /// <summary>
        /// Aggiunge alla datagridview tutti i luoghi della tabella Luogo
        /// </summary>
        private void AggiornaDGV(List<Luogo> l)
        {
            dgwluogo.Rows.Clear();
            foreach (var item in l)
                dgwluogo.Rows.Add(item.IDLuogo, item.Nazione, item.Citta, item.Indirizzo, item.Moneta, item.Lingua);
        }

        /// <summary>
        /// Ritorna il dato del luogo data in input la cella del datagridview
        /// </summary>
        private string DatoLuogo(int index)
        {
            return dgwluogo[index, dgwluogo.SelectedCells[0].RowIndex].Value.ToString();
        }

        /// <summary>
        /// Aggiorno il database in base ai contenuti modificati in datagridview
        /// </summary>
        private void dgwluogo_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                List<Luogo> l = new List<Luogo>();
                l.Add(new Luogo(Row(e.RowIndex)));
                TLuogo.Update(l);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string[] Row(int index)
        {
            return new string[] { dgwluogo[0, index].Value.ToString(), dgwluogo[1, index].Value.ToString(), dgwluogo[2, index].Value.ToString(), dgwluogo[3, index].Value.ToString(), dgwluogo[4, index].Value.ToString(), dgwluogo[5, index].Value.ToString() };
        }

        private void mappaToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            List<Luogo> l = new List<Luogo>();
            l.Add(new Luogo(DatoLuogo(4), DatoLuogo(5), DatoLuogo(3), DatoLuogo(1), DatoLuogo(2), DatoLuogo(0)));
            new frmMappa(l).ShowDialog();
        }

        private void visualizzaTutteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                AggiornaDGV(TLuogo.Read());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void visualizzaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel2.Visible = true;
            panel2.Enabled = true;
            IdLuogo = DatoLuogo(0);
            RdoRistoranti.Checked = true;
        }

        private void inserisciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmAggiungiDestinazione(new Luogo(DatoLuogo(0), DatoLuogo(4), DatoLuogo(5), DatoLuogo(3), DatoLuogo(1), DatoLuogo(2))).Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var SenderGrid = (DataGridView)sender;
            if (SenderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0 && dataGridView1[e.ColumnIndex, e.RowIndex].Value.ToString() != "Non inserito")
            {
                AggiornaDGV(TLuogo.Cerca(dataGridView1[e.ColumnIndex, e.RowIndex].Value.ToString()));
                VisualizzaPannelli(true, panel1);
                VisualizzaPannelli(false, panel2, panel4);
            }
        }

        /// <summary>
        /// Crea un oggetto Hotel, come parametri vuole l'index della gridview dove andare a prendere i dati
        /// </summary>
        private VittoAlloggio RowH(int index)
        {
            return new VittoAlloggio(dgvVittoAlloggio[0, index].Value.ToString(), dgvVittoAlloggio[1, index].Value.ToString(), dgvVittoAlloggio[2, index].Value.ToString(), dgvVittoAlloggio[3, index].Value.ToString());
        }

        /// <summary>
        /// Crea un oggetto Ristorante, come parametri vuole l'index della gridview dove andare a prendere i dati
        /// </summary>
        private VittoAlloggio RowR(int index)
        {
            return new VittoAlloggio(dgvVittoAlloggio[0, index].Value.ToString(), dgvVittoAlloggio[1, index].Value.ToString(), dgvVittoAlloggio[2, index].Value.ToString(), dgvVittoAlloggio[3, index].Value.ToString());
        }

        /// <summary>
        /// Visualizzo i risotranti nella DataGridView
        /// </summary>
        private void RdoRistoranti_CheckedChanged_1(object sender, EventArgs e)
        {
            try
            {
                dgvVittoAlloggio.Rows.Clear();

                List<VittoAlloggio> l = TRistorante.Read(IdLuogo);
                foreach (var item in l)
                    dgvVittoAlloggio.Rows.Add(item.IDVittoAlloggio, item.IDLuogo, item.Nome, item.Indirizzo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        ///  Visualizzo gli hotel nella DataGridView
        /// </summary>
        private void rdoHotel_CheckedChanged_1(object sender, EventArgs e)
        {
            try
            {
                dgvVittoAlloggio.Rows.Clear();

                List<VittoAlloggio> l = THotel.Read(IdLuogo);
                foreach (var item in l)
                    dgvVittoAlloggio.Rows.Add(item.IDVittoAlloggio, item.IDLuogo, item.Nome, item.Indirizzo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Visualizzo l'hotel o il ristorante selezionato nella mappa
        /// </summary>
        private void btnMappa_Click_1(object sender, EventArgs e)
        {
            try
            {
                new frmMappa(TLuogo.Cerca(dgvVittoAlloggio[1, dgvVittoAlloggio.SelectedCells[0].RowIndex].Value.ToString())).Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        ///  Permette di modificare gli hotel e i ristoranti direttamente dalla DataGridView salvando i dati sul database
        /// </summary>
        private void dgvVittoAlloggio_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (rdoHotel.Checked)
                {
                    List<VittoAlloggio> l = new List<VittoAlloggio>();
                    l.Add(RowH(e.RowIndex));
                    THotel.Update(l);
                }

                if (RdoRistoranti.Checked)
                {
                    List<VittoAlloggio> l = new List<VittoAlloggio>();
                    l.Add(RowR(e.RowIndex));
                    TRistorante.Update(l);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void VisualizzaPannelli(bool Visibile, params Panel[] p)
        {
            foreach (var item in p)
            {
                item.Visible = Visibile;
                item.Enabled = Visibile;
            }
        }


        /// <summary>
        /// Aggiorna la datagridview col luogo selezionato dalla combobox
        /// </summary>
        private void cmbluogo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                AggiornaDGV(TLuogo.Cerca(cmbluogo.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnChiudiVittoAlloggio_Click(object sender, EventArgs e)
        {
            VisualizzaPannelli(false, panel4, panel3, panel2);
            VisualizzaPannelli(true, panel1);
        }

        private void btnCloseCommesseCollaboratore_Click(object sender, EventArgs e)
        {
            VisualizzaPannelli(false, panel2, panel3, panel4);
            VisualizzaPannelli(true, panel4);
        }

        private void btnChiudiVisDestinazioni_Click(object sender, EventArgs e)
        {
            VisualizzaPannelli(false, panel1, panel2);
            VisualizzaPannelli(true, panel4);
        }

        private void dgwluogo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (panel2.Visible == true)
            {
                IdLuogo = DatoLuogo(0);
                RdoRistoranti.Checked = false;
                RdoRistoranti.Checked = true;
            }
        }

        private void cmbDestinazione_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbDestinazione.BackColor = Color.GreenYellow;
        }
    }
}