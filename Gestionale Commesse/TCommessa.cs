﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Gestionale_Commesse
{
    /// <summary>
    /// Classe contenente tutte le funzioni per la gestione della tabella commessa
    /// </summary>
    static class TCommessa
    {
        static string connString = Properties.Settings.ConnectionString;

        /// <summary>
        /// Funzione che salva i dati sulla tabella, come parametro richiede una variabile commessa 
        /// </summary>
        public static void Update(Commessa c)
        {
            string cmdString = "IF EXISTS (SELECT * FROM Commessa WHERE IDCommessa = @IDCommessa)" +
                " UPDATE Commessa SET IDCommessa = @IDCommessa, Cliente = @Cliente, Data = @Data, IDLuogo = @IDLuogo" +
                " WHERE IDCommessa = @IDCommessa" +
                " ELSE" +
                " INSERT INTO Commessa (IDCommessa, Cliente, Data, IDLuogo) VALUES (@IDCommessa, @Cliente, @Data, @IDLuogo)";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                cmd.Parameters.AddWithValue("@IDCommessa", c.IDCommessa);
                cmd.Parameters.AddWithValue("@Cliente", c.Cliente);
                cmd.Parameters.AddWithValue("@Data", c.Data);
                cmd.Parameters.AddWithValue("@IDLuogo", c.IDLuogo);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        /// <summary>
        /// Funzione che elimina una commessa dalla tabella in base all'ID, come parametri richiede una stringa che rappresenta l'ID
        /// </summary>
        public static void Delete(string IDCommessa)
        {
            TPartecipa.Delete(IDCommessa);
            string cmdString = "DELETE FROM Commessa WHERE IDCommessa=@IDCommessa";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                cmd.Parameters.AddWithValue("@IDCommessa", IDCommessa);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        /// <summary>
        /// Funzione che conta il numero delle righe della tabella, non vuole nessun parametro
        /// ritorna il numero di righe se non si verifica nessun problema, ritorna -1 se si verifica un eccezione
        /// </summary>
        public static int RowCount()
        {
            try
            {
                string cmdString = "SELECT COUNT(*) FROM Commessa";
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = cmdString;
                    int n = (int)cmd.ExecuteScalar();
                    conn.Close();
                    return n;
                }
            }
            catch { return -1; }
        }

        /// <summary>
        /// Funzione che legge tutti i dati dalla tabella Commessa, ritorna una lista di Commesse
        /// </summary>
        public static List<Commessa> Read()
        {
            List<Commessa> l = new List<Commessa>();
            string cmdString = "SELECT * FROM Commessa";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(new Commessa(reader["IDCommessa"].ToString(), reader["Cliente"].ToString(), Convert.ToDateTime(reader["Data"]), reader["IDLuogo"].ToString()));
                conn.Close();
                return l;
            }
        }

        /// <summary>
        /// Funzione che ritorna una lista di stinghe contentente gli id delle varie commesse
        /// </summary>
        public static List<string> IDCommessa()
        {
            List<string> l = new List<string>();
            string cmdString = "SELECT IDCommessa FROM Commessa";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(reader["IDCommessa"].ToString());
                conn.Close();
                return l;
            }
        }

        /// <summary>
        /// Funzione che ritorna una lista di commesse, come parametri vuole l'id della commessa da cercare
        /// </summary>
        public static List<Commessa> Read(string IdCommessa)
        {
            List<Commessa> l = new List<Commessa>();
            string cmdString = "SELECT * FROM Commessa WHERE IDCommessa=@IDCommessa";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                cmd.Parameters.AddWithValue("@IDCommessa", IdCommessa);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    l.Add(new Commessa(reader["IDCommessa"].ToString(), reader["Cliente"].ToString(), Convert.ToDateTime(reader["Data"]), reader["IDLuogo"].ToString()));
                conn.Close();
                return l;
            }
        }

        /// <summary>
        /// Funzione che ritorna il nome della commessa dato l'id del luogo
        /// </summary>
        public static List<Commessa> TrovaLuogo(string IDLuogo)
        {
            List<Commessa> l = new List<Commessa>();
            string cmdString = "SELECT * FROM Commessa WHERE Commessa.IDLuogo=@IDLuogo";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                cmd.Parameters.AddWithValue("@IDLuogo", IDLuogo);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    l.Add(new Commessa(reader["IDCommessa"].ToString(), reader["Cliente"].ToString(), Convert.ToDateTime(reader["Data"]), reader["IDLuogo"].ToString()));
                conn.Close();
                return l;
            }
        }
        public static bool Empty
        {
            get { return RowCount() <= 0; }
        }

    }
}