﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Gestionale_Commesse
{
    public partial class frmAggiungiDestinazione : Form
    {
        /// <summary>
        /// Lista temporanea dei ristoranti che andranno salvati dopo il click del bottone "btnNuovo"
        /// </summary>
        List<VittoAlloggio> lr = new List<VittoAlloggio>();

        /// <summary>
        /// Lista temporanea degli hotel che andranno salvati dopo il click del bottone "btnNuovo"
        /// </summary>
        List<VittoAlloggio> lh = new List<VittoAlloggio>();

        Luogo l;
        public frmAggiungiDestinazione()
        {
            InitializeComponent();
        }
        public frmAggiungiDestinazione(Luogo l)
        {
            InitializeComponent();
            this.l = l;
            txtCitta.ReadOnly = true;
            txtIdLuogo.ReadOnly = true;
            txtIndirizzo.ReadOnly = true;
            txtLingua.ReadOnly = true;
            txtmoneta.ReadOnly = true;
            txtNazione.ReadOnly = true;
            btnAggiungiHotel.Enabled = false;
            btnAggiungiRistoranti.Enabled = false;
            btnnuovo.Text = "Salva";
        }

        /// <summary>
        /// Funzione che aggiunge una nuova destinazione per la commessa nella tabella Luogo, salva anche eventuali ristoranti e hotel inseriti
        /// </summary>
        private void btnnuovo_Click(object sender, EventArgs e)
        {
            try
            {
                List<Luogo> l = new List<Luogo>();
                l.Add(new Luogo(txtIdLuogo.Text, txtmoneta.Text, txtLingua.Text, txtIndirizzo.Text, txtNazione.Text, txtCitta.Text));

                // Imposto l'idluogo ai ristoranti e agli hotel
                foreach (var item in lr)
                    item.IDLuogo = l[0].IDLuogo;
                foreach (var item in lh)
                    item.IDLuogo = l[0].IDLuogo;

                //Salvo i dati sulle tabelle
                TLuogo.Update(l);
                TRistorante.Update(lr);
                THotel.Update(lh);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Pulisci();
            if (btnnuovo.Text == "Salva") Close();
        }

        /// <summary>
        /// Funzione che pulisce il contenuto delle textbox e le liste dei ristoranti ed hotel
        /// </summary>
        private void Pulisci()
        {
            lr.Clear();
            lh.Clear();
            PulisciTextBox(txtCitta, txtHotel, txtIndirizRistorante, txtIndirizzo, txtIndirizzoHotel, txtLingua, txtmoneta, txtNazione, txtNristorante,txtIdLuogo);
        }

        private void btnChiudi_Click(object sender, EventArgs e)
        {
            Close();
        }

        void PulisciTextBox(params TextBox[] tx)
        {
            foreach (var item in tx)
                item.Clear();
        }

        /// <summary>
        /// Aggiunta del ristorante alla lista dei ristoranti
        /// </summary>
        private void btnAggiungi_Click(object sender, EventArgs e)
        {
            lr.Add(new VittoAlloggio(txtNristorante.Text, txtIndirizRistorante.Text));
            PulisciTextBox(txtNristorante, txtIndirizRistorante);
        }

        /// <summary>
        /// Aggiunta degli hotel alla lista degli hotel
        /// </summary>
        private void btnAggiungiHotel_Click(object sender, EventArgs e)
        {
            lh.Add(new VittoAlloggio(txtHotel.Text, txtIndirizzoHotel.Text));
            PulisciTextBox(txtIndirizzoHotel, txtHotel);
        }

        private void frmAggiungiDestinazione_Load(object sender, EventArgs e)
        {
            if (l == null)
            {
                btnnuovo.Enabled = false;
                btnAggiungiHotel.Enabled = false;
                btnAggiungiRistoranti.Enabled = false;
            }
            else
            {
                txtCitta.Text = l.Citta;
                txtIdLuogo.Text = l.IDLuogo;
                txtIndirizzo.Text = l.Indirizzo;
                txtLingua.Text = l.Lingua;
                txtmoneta.Text = l.Moneta;
                txtNazione.Text = l.Nazione;
            }
        }

        private void txtmoneta_TextChanged(object sender, EventArgs e)
        {
            Abilita();
        }


        /// <summary>
        /// Controllo della presenza dei dati nelle caselle di testo prima di abilitare o disabilitare il bottone "btnNuovo"
        /// </summary>
        void Abilita()
        {
            if (string.IsNullOrWhiteSpace(txtCitta.Text) || string.IsNullOrWhiteSpace(txtIndirizzo.Text) || string.IsNullOrWhiteSpace(txtLingua.Text)
                || string.IsNullOrWhiteSpace(txtmoneta.Text) || string.IsNullOrWhiteSpace(txtNazione.Text))
                btnnuovo.Enabled = false;
            else
                btnnuovo.Enabled = true;
        }

        /// <summary>
        /// Controllo della presenza dei dati dell'hotel nelle caselle di testo prima di abilitare il bottone btnAggiungiHotel
        /// </summary>
        void AbilitaH()
        {
            if (string.IsNullOrWhiteSpace(txtHotel.Text) || string.IsNullOrWhiteSpace(txtIndirizzoHotel.Text))
                btnAggiungiHotel.Enabled = false;
            else
                btnAggiungiHotel.Enabled = true;
        }

        /// <summary>
        /// Controllo della presenza dei dati del ristorante nelle caselle di testo prima di abilitare il bottone btnAggiungiRistorante
        /// </summary>
        void AbilitaR()
        {
            if (string.IsNullOrWhiteSpace(txtIndirizRistorante.Text) || string.IsNullOrWhiteSpace(txtNristorante.Text))
                btnAggiungiRistoranti.Enabled = false;
            else
                btnAggiungiRistoranti.Enabled = true;
        }

        private void txtHotel_TextChanged(object sender, EventArgs e)
        {
            Abilita();
            AbilitaH();
        }

        private void txtNazione_TextChanged(object sender, EventArgs e)
        {
            Abilita();
        }

        private void txtCitta_TextChanged(object sender, EventArgs e)
        {
            Abilita();
        }

        private void txtIndirizzo_TextChanged(object sender, EventArgs e)
        {
            Abilita();
        }

        private void txtIndirizzoHotel_TextChanged(object sender, EventArgs e)
        {
            Abilita();
            AbilitaH();
        }

        private void txtNristorante_TextChanged(object sender, EventArgs e)
        {
            Abilita();
            AbilitaR();
        }

        private void txtIndirizRistorante_TextChanged(object sender, EventArgs e)
        {
            Abilita();
            AbilitaR();
        }

        private void txtLingua_TextChanged(object sender, EventArgs e)
        {
            Abilita();
        }
    }

}