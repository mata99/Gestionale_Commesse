﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace Gestionale_Commesse
{
    /// <summary>
    /// Classe contenente tutte le funzioni per la gestione della tabella Hotel
    /// </summary>
    static class THotel
    {
        static string connString = Properties.Settings.ConnectionString;

        /// <summary>
        /// Funzione che salva una lista di hotel nella tabella Hotel
        /// </summary>
        public static void Save(List<VittoAlloggio> lh)
        {
            string cmdString = "INSERT INTO Hotel (IDHotel,IdLuogo,Nome,Indirizzo) " + "VALUES (@IDHotel,@IdLuogo,@Nome,@Indirizzo)";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();

                foreach (var item in lh)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = cmdString;
                    cmd.Parameters.AddWithValue("@IDHotel", item.IDVittoAlloggio);
                    cmd.Parameters.AddWithValue("@IdLuogo", item.IDLuogo);
                    cmd.Parameters.AddWithValue("@Nome", item.Nome);
                    cmd.Parameters.AddWithValue("@Indirizzo", item.Indirizzo);
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        /// <summary>
        /// Funzione che conta le righe della tabella Hotel, ritorna -1 in caso si verifichino degli errori
        /// </summary>
        public static int RowCount()
        {
            try
            {
                string cmdString = "SELECT COUNT(*) FROM Hotel";
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = cmdString;
                    int n = (int)cmd.ExecuteScalar();
                    conn.Close();
                    return n;
                }
            }
            catch { return -1; }
        }

        /// <summary>
        /// Funzione che legge una lista di Hotel di un determinato luogo dalla tabella Hotel, coma parametri richiede l'id del luogo, ritorna una lista di Hotel
        /// </summary>
        public static List<VittoAlloggio> Read(string IDLuogo)
        {
            List<VittoAlloggio> l = new List<VittoAlloggio>();
            string cmdString = "SELECT * FROM Hotel WHERE IDLuogo=@IDLuogo";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                cmd.Parameters.AddWithValue("@IDLuogo", IDLuogo);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(new VittoAlloggio(reader["IDLuogo"].ToString(), reader["Nome"].ToString(), reader["Indirizzo"].ToString()));
                conn.Close();
            }
            return l;
        }

        /// <summary>
        /// Funzione che legge una lista di tipo Ristorante di un determinato luogo dalla tabella Ristorante
        /// </summary>
        public static List<VittoAlloggio> ReadAll()
        {
            List<VittoAlloggio> l = new List<VittoAlloggio>();
            string cmdString = "SELECT * FROM Hotel";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(new VittoAlloggio(reader["IDLuogo"].ToString(), reader["Nome"].ToString(), reader["Indirizzo"].ToString()));
                conn.Close();
            }
            return l;
        }

        public static bool Empty
        {
            get { return RowCount() <= 0; }
        }

        /// <summary>
        /// Funzione che aggiorna la tabella Hotel aggiungendone dei nuovi o sovrascrivendone alcuni esistenti, come parametri richiede una lista di hotel
        /// </summary>
        public static void Update(List<VittoAlloggio> lh)
        {
            string cmdString = "IF EXISTS (SELECT * FROM Hotel WHERE IDLuogo=@IDLuogo AND IDHotel = @IDHotel)" +
                " UPDATE Hotel SET IDHotel = @IDHotel, IDLuogo = @IDLuogo, Nome = @Nome, Indirizzo = @Indirizzo" +
                " WHERE IDLuogo = @IDLuogo AND IDHotel = @IDHotel ELSE INSERT INTO Hotel (IDHotel, IDLuogo, Nome, Indirizzo) VALUES (@IDHotel, @IDLuogo, @Nome, @Indirizzo)";
            SqlConnection conn = new SqlConnection(connString);
            conn.Open();
            foreach (var h in lh)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                cmd.Parameters.AddWithValue("@IDLuogo", h.IDLuogo);
                cmd.Parameters.AddWithValue("@Nome", h.Nome);
                cmd.Parameters.AddWithValue("@Indirizzo", h.Indirizzo);
                cmd.Parameters.AddWithValue("@IDHotel", h.IDVittoAlloggio);
                cmd.ExecuteNonQuery();
            }
            conn.Close();
        }
    }
}