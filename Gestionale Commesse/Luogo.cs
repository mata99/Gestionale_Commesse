﻿namespace Gestionale_Commesse
{
    /// <summary>
    /// Classe per la gestione dei dati di un luogo.
    /// </summary>
    public class Luogo
    {
        public string IDLuogo { get; set; }
        public string Moneta { get; private set; }
        public string Lingua { get; private set; }
        public string Indirizzo { get; private set; }
        public string Nazione { get; private set; }
        public string Citta { get; private set; }

        /// <summary>
        /// Costruttore della classe Luogo: richiede l'ID del luogo
        /// </summary>
        public Luogo(string IDLuogo, string Moneta, string Lingua, string Indirizzo, string Nazione, string Citta)
        {
            this.IDLuogo = IDLuogo;
            this.Moneta = Moneta;
            this.Lingua = Lingua;
            this.Indirizzo = Indirizzo;
            this.Nazione = Nazione;
            this.Citta = Citta;
        }

        public Luogo(string[] parametri)
        {
            IDLuogo = parametri[0];
            Nazione = parametri[1];
            Citta = parametri[2];
            Indirizzo = parametri[3];
            Moneta = parametri[4];
            Lingua = parametri[5];
        }
    }
}
