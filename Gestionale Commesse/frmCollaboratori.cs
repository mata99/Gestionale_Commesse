﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net;
using System.Threading;
using System.Windows.Forms;

namespace Gestionale_Commesse
{
    public partial class frmCollaboratori : Form
    {
        private string user;
        private string pw;
        public frmCollaboratori()
        {
            InitializeComponent();
            cmbCognome.AutoCompleteSource = AutoCompleteSource.ListItems;
            cmbCognome.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            dgwvisualizza.CellValueChanged += new DataGridViewCellEventHandler(dgwvisualizza_CellValueChanged);
        }

        /// <summary>
        /// Rimando alla form AggiungiCollaboratore
        /// </summary>
        private void aggiungiCollaboratoriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                new frmAggiungiCollaboratore().ShowDialog();
                AggiornaDGV(TCollaboratore.Read());
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        /// <summary>
        /// Avvio la procedura per il download dei collaboratori dal sito
        /// </summary>
        private void aggiornaCollaboratoriToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                List<Collaboratore> l;
                Thread thread = new Thread(t =>
                    {
                        l = TCollaboratore.DownloadCollaboratori(user, pw); // Scarico i collaboratori dal sito
                        TCollaboratore.UpdateAvanzato(l);
                        AggiornaDGV(TCollaboratore.Read()); // Aggiorno la datagridview
                        AggiornaCMB(TCollaboratore.Cognomi());
                    })
                { IsBackground = true };
                thread.Start();
                CheckForIllegalCrossThreadCalls = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Rimando alla form GestisciCommessa per aggiungere, visualizzare e modificare le commesse
        /// </summary>
        private void gestisciCommesseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmGestisciCommessa().ShowDialog();
        }

        /// <summary>
        /// Aggiorno la datagridview con i collaboratori passati per parametro attraverso una lista
        /// </summary>
        private void AggiornaDGV(List<Collaboratore> l)
        {
            dgwvisualizza.Rows.Clear();

            foreach (var item in l)
                dgwvisualizza.Rows.Add(item.Cognome, item.Nome, item.Reparto, item.Email, item.TelInterno, item.Cellulare, item.TipoDiCollaborazione, item.DataAssunzione, item.Comune, item.IDCollaboratore);

            dgwvisualizza.Sort(Column1, System.ComponentModel.ListSortDirection.Ascending);
            dgwvisualizza[0, 0].Selected = true;

            foreach (DataGridViewRow item in dgwvisualizza.Rows)
                if (item.Cells[6].Value.ToString() == "Cessata")
                    item.DefaultCellStyle.BackColor = Color.LightCoral;
        }

        /// <summary>
        /// Aggiorno il combobox per la ricerca tramite cognomi
        /// </summary>
        private void AggiornaCMB(List<string> Cognomi)
        {
            try
            {
                cmbCognome.SelectedIndex = -1;
                cmbCognome.Items.Clear();
                cmbCognome.Items.AddRange(TCollaboratore.Cognomi().ToArray());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Visualizzazione di tutti i collaboratori presenti in database
        /// </summary>
        private void btnCerca_Click(object sender, EventArgs e)
        {
            try
            { AggiornaDGV(TCollaboratore.Read()); }
            catch (Exception ex)
            { MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        /// <summary>
        /// Avvia la procedura per la ricerca di un collaboratore attraverso il suo cognome
        /// </summary>
        private void cmbCognome_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            { AggiornaDGV(TCollaboratore.Cerca(cmbCognome.Text)); }
            catch (Exception ex)
            { MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        /// <summary>
        /// Aggiorno la datagridview e il combobox dei cognomi
        /// </summary>
        private void frmCollaboratori_Load(object sender, EventArgs e)
        {
            Abilita();
            Gestisci(false, menuStrip1, cmbCognome, btnVisualizzaTutti, btnElimina, dgwvisualizza, btnLogout);
            try
            {
                if (!TCollaboratore.Empty)
                {
                    AggiornaDGV(TCollaboratore.Read());
                    AggiornaCMB(TCollaboratore.Cognomi());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Funzione che gestisce l'inserimento delle credenziali per l'accesso al programma
        /// </summary>
        private bool CambiaUtente()
        {
            user = txtUser.Text;
            pw = txtPW.Text;

            if (DatiCorretti())
            {
                txtUser.Text = user;
                Enabled = true;
                return true;
            }
            else
            {
                txtPW.Clear();
                Abilita();
                Gestisci(false, menuStrip1, cmbCognome, btnVisualizzaTutti, btnElimina, dgwvisualizza);
                return false;
            }

        }

        /// <summary>
        /// Funzione che verifica se le credenziali inserire sono corrette
        /// ritorna true se i dati sono corretti, false se non lo sono
        /// </summary>
        private bool DatiCorretti()
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.Credentials = new NetworkCredential(user, pw);
                    client.OpenRead("http://intranet.loccioni.com");
                    return true;
                }
            }
            catch
            { return false; }
        }

        /// <summary>
        /// Avvio la procedura per il settaggio della collaborazione "cessata" di un collaboratore
        /// </summary>
        private void btnElimina_Click(object sender, EventArgs e)
        {
            try
            {
                TCollaboratore.CessataCollaborazione(dgwvisualizza[9, dgwvisualizza.SelectedCells[0].RowIndex].Value.ToString());
                AggiornaDGV(TCollaboratore.Read());
                AggiornaCMB(TCollaboratore.Cognomi());
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }



        /// <summary>
        /// Procedura di update di una riga della tabella Collaboratore nel caso in cui il collaboratore sia stato modificato
        /// tramite datagridview
        /// </summary>
        private void dgwvisualizza_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                List<Collaboratore> l = new List<Collaboratore>();
                l.Add(new Collaboratore(Row(e.RowIndex)));
                TCollaboratore.Update(l);
                AggiornaDGV(TCollaboratore.Read());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Funzione che restituisce sotto forma di array di stringhe il contenuto di una riga della datagridview
        /// </summary>
        string[] Row(int index)
        {
            return new string[] { dgwvisualizza[0, index].Value.ToString(), dgwvisualizza[1, index].Value.ToString(), dgwvisualizza[2, index].Value.ToString(), dgwvisualizza[3, index].Value.ToString(), dgwvisualizza[4, index].Value.ToString(), dgwvisualizza[5, index].Value.ToString(), dgwvisualizza[6, index].Value.ToString(), dgwvisualizza[7, index].Value.ToString(), dgwvisualizza[8, index].Value.ToString(), dgwvisualizza[9, index].Value.ToString() };
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(t =>
            {
                if (CambiaUtente())
                {
                    Gestisci(true, menuStrip1, cmbCognome, btnVisualizzaTutti, btnElimina, dgwvisualizza, btnLogout);
                    Gestisci(false, btnLogin);
                    MessageBox.Show("Accesso eseguito!", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtPW.ReadOnly = true;
                    txtUser.ReadOnly = true;
                }
                else
                    MessageBox.Show("Credenziali non valide!", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);
            })
            { IsBackground = true };
            thread.Start();
            CheckForIllegalCrossThreadCalls = false;
        }

        private void txtUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                btnLogin.PerformClick();
        }

        private void txtPW_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                btnLogin.PerformClick();
        }

        private void Abilita()
        {
            if (!string.IsNullOrEmpty(txtPW.Text) && !string.IsNullOrEmpty(txtUser.Text))
                btnLogin.Enabled = true;
            else
                btnLogin.Enabled = false;
        }

        private void frmCollaboratori_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(user))
            MessageBox.Show("Devi eseguire il login","Attenzione",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }
        private void Gestisci(bool b, params Control [] v)
        {
            foreach (var item in v)
                item.Enabled = b;
        }

        private void txtUser_TextChanged(object sender, EventArgs e)
        {
            Abilita();
        }
        private void txtPW_TextChanged(object sender, EventArgs e)
        {
            Abilita();
        }

        private void aggiungiLuoghiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmAggiungiDestinazione().ShowDialog();
        }

        private void visualizzaTuttoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmMappa(TLuogo.Commesse(),TRistorante.ReadAll(),THotel.ReadAll()).ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            Abilita();
            Gestisci(false, menuStrip1, cmbCognome, btnVisualizzaTutti, btnElimina, dgwvisualizza, btnLogout);
            txtPW.ReadOnly = false;
            txtUser.ReadOnly = false;
            txtUser.Clear();
            txtPW.Clear();
        }

    }
}