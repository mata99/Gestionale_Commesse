﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Gestionale_Commesse
{
    /// <summary>
    /// Classe contenente tutte le funzioni per la gestione della tabella intermedia CollaboratoriInCommessa
    /// </summary>
    static class TPartecipa
    {
        static string connString = Properties.Settings.ConnectionString;

        /// <summary>
        /// Funzione che aggiorna la tabella Partecipa aggiungendo nuovi collaboratori o eliminandoli, come parametri richiede una lista di hotel 
        /// </summary>
        public static void Update(List<string> IdCollaboratori/*Collaboratori presenti*/, List<string> IdNonCl, string IdCommessa)
        {
            // Se non esiste lo inserisce
            string cmdString = "IF NOT EXISTS (SELECT * FROM CollaboratoriInCommessa WHERE Id_cm=@Id_cm AND Id_cl=@Id_cl)" +
                " INSERT INTO CollaboratoriInCommessa (Id_cm,Id_cl) VALUES (@Id_cm,@Id_cl)";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                foreach (var item in IdCollaboratori)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = cmdString;
                    cmd.Parameters.AddWithValue("@Id_cm", IdCommessa);
                    cmd.Parameters.AddWithValue("@Id_cl", item);
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }

            Cancella_cl(IdNonCl, IdCommessa);

        }

        /// <summary>
        /// Funzione che cancella i collaboratori che non partecipano più ad una commessa, come parametri vuole una lista di stringhe
        /// che rappresenta l'id dei collaboratori e una stringa che rappresenta l'id della commessa
        /// </summary>
        private static void Cancella_cl(List<string> l, string IdCommessa)
        {
            string cmdString = "DELETE FROM CollaboratoriInCommessa WHERE Id_cm=@Id_cm AND Id_cl=@Id_cl";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                foreach (var item in l)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = cmdString;
                    cmd.Parameters.AddWithValue("@Id_cm", IdCommessa);
                    cmd.Parameters.AddWithValue("@Id_cl", item);
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        /// <summary>
        /// Funzione che cancella una commessa dalla tabella CollaboratoriInCommessa, come parametro vuole l'id della commessa
        /// </summary>
        public static void Delete(string IdCommessa)
        {
            string cmdString = "DELETE FROM CollaboratoriInCommessa WHERE Id_cm=@c";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                cmd.Parameters.AddWithValue("@c", IdCommessa);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        /// <summary>
        /// Funzione che conta le righe della tabella CollaboratoriInCommessa, ritorna -1 in caso si verifichino degli errori
        /// </summary>
        public static int RowCount()
        {
            try
            {
                string cmdString = "SELECT COUNT(*) FROM CollaboratoriInCommessa";
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = cmdString;
                    int n = (int)cmd.ExecuteScalar();
                    conn.Close();
                    return n;
                }
            }
            catch
            {
                return -1;
            }
        }

        /// <summary>
        /// Funzione che restituisce il numero delle commesse in cui è impegnato un collaboratore; richiede come parametro
        /// una stringa rappresentante l'ID del collaboratore
        /// </summary>
        public static int Commesse_Collaboratore(string Cod_cl)
        {
            string cmdString = "SELECT COUNT(*) FROM CollaboratoriInCommessa WHERE Id_cl=@Id_cl";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                cmd.Parameters.AddWithValue("@Id_cl", Cod_cl);
                int n = (int)cmd.ExecuteScalar();
                conn.Close();
                return n;
            }
        }

        /// <summary>
        /// Funzione che restituisce la lista dei collaboratori che partecipano alla creazione di una commessa; come parametro richiede una stringa rappresentante l'ID della commessa
        /// </summary>
        public static List<Collaboratore> PartecipaCommessa(string cod_commessa)
        {
            List<Collaboratore> l = new List<Collaboratore>();
            string cmdString = "SELECT * FROM Collaboratore, CollaboratoriInCommessa WHERE Collaboratore.Id_cl=CollaboratoriInCommessa.Id_cl AND CollaboratoriInCommessa.Id_cm=@Cod_cm";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                cmd.Parameters.AddWithValue("@Cod_cm", cod_commessa);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    l.Add(new Collaboratore(reader["Nome"].ToString(), reader["Cognome"].ToString(), reader["Reparto"].ToString(), reader["Email"].ToString(), reader["TelInterno"].ToString(), reader["Cellulare"].ToString(), reader["TipoColl"].ToString(), reader["DataAss"].ToString(), reader["Comune"].ToString(), reader["Id_cl"].ToString()));
                return l;
            }
        }

        /// <summary>
        /// Funzione che restituisce una lista di commesse, contenente la commessa corrispondente all'ID passato come parametro
        /// </summary>
        public static List<Commessa> Dettagli_Commessa(string cod_cl)
        {
            List<Commessa> l = new List<Commessa>();
            string cmdString = "SELECT * FROM Commessa, CollaboratoriInCommessa WHERE IDCommessa=Id_cm AND Id_cl=@Cod_cl";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                cmd.Parameters.AddWithValue("@Cod_cl", cod_cl);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    l.Add(new Commessa(reader["IDCommessa"].ToString(), reader["Cliente"].ToString(), Convert.ToDateTime(reader["Data"]), reader["IDLuogo"].ToString()));
                return l;
            }
        }

        public static bool Empty
        {
            get { return RowCount() <= 0; }
        }
    }
}