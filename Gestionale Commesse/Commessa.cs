﻿using System;

namespace Gestionale_Commesse
{
    /// <summary>
    /// Classe per la gestione dei dati di una Commessa.
    /// </summary>
    public class Commessa
    {
        public string IDCommessa { get; private set; }
        public string Cliente { get; private set; }
        public DateTime Data { get; private set; }
        public string IDLuogo { get; private set; }

        /// <summary>
        /// Costruttore della classe Commessa
        /// Richiede l'ID del luogo
        /// </summary>
        public Commessa(string IDCommessa, string Cliente, DateTime Data, string IDLuogo)
        {
            this.IDCommessa = IDCommessa;
            this.Cliente = Cliente;
            if (Data > DateTime.Today)
                this.Data = Data;
            else
                throw new Exception("Data non corretta!");
            this.IDLuogo = IDLuogo;
        }
    }
}
