﻿namespace Gestionale_Commesse
{
    partial class frmCollaboratori
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCollaboratori));
            this.dgwvisualizza = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbCognome = new System.Windows.Forms.ComboBox();
            this.lblcognome = new System.Windows.Forms.Label();
            this.btnElimina = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.aggiornaCollaboratoriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aggiornaCollaboratoriToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aggiungiCollaboratoriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestisciCommesseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestisciLuoghiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aggiungiLuoghiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visualizzaTuttoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnVisualizzaTutti = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtPW = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnLogout = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgwvisualizza)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgwvisualizza
            // 
            this.dgwvisualizza.AllowUserToAddRows = false;
            this.dgwvisualizza.AllowUserToResizeRows = false;
            this.dgwvisualizza.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgwvisualizza.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgwvisualizza.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwvisualizza.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10});
            this.dgwvisualizza.Location = new System.Drawing.Point(12, 98);
            this.dgwvisualizza.MultiSelect = false;
            this.dgwvisualizza.Name = "dgwvisualizza";
            this.dgwvisualizza.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgwvisualizza.Size = new System.Drawing.Size(1223, 524);
            this.dgwvisualizza.TabIndex = 6;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Cognome";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Nome";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Reparto";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Email";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Telefono interno";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Cellulare";
            this.Column6.Name = "Column6";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Tipo di collaborazione";
            this.Column7.Name = "Column7";
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Data assunzione";
            this.Column8.Name = "Column8";
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Residenza";
            this.Column9.Name = "Column9";
            // 
            // Column10
            // 
            this.Column10.HeaderText = "ID";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // cmbCognome
            // 
            this.cmbCognome.FormattingEnabled = true;
            this.cmbCognome.Location = new System.Drawing.Point(73, 53);
            this.cmbCognome.Name = "cmbCognome";
            this.cmbCognome.Size = new System.Drawing.Size(190, 21);
            this.cmbCognome.Sorted = true;
            this.cmbCognome.TabIndex = 5;
            this.cmbCognome.SelectedIndexChanged += new System.EventHandler(this.cmbCognome_SelectedIndexChanged);
            // 
            // lblcognome
            // 
            this.lblcognome.AutoSize = true;
            this.lblcognome.Location = new System.Drawing.Point(11, 56);
            this.lblcognome.Name = "lblcognome";
            this.lblcognome.Size = new System.Drawing.Size(52, 13);
            this.lblcognome.TabIndex = 4;
            this.lblcognome.Text = "Cognome";
            // 
            // btnElimina
            // 
            this.btnElimina.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnElimina.Location = new System.Drawing.Point(1099, 69);
            this.btnElimina.Name = "btnElimina";
            this.btnElimina.Size = new System.Drawing.Size(134, 23);
            this.btnElimina.TabIndex = 1;
            this.btnElimina.Text = "Cessa collaborazione";
            this.btnElimina.UseVisualStyleBackColor = true;
            this.btnElimina.Click += new System.EventHandler(this.btnElimina_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aggiornaCollaboratoriToolStripMenuItem,
            this.gestisciCommesseToolStripMenuItem,
            this.gestisciLuoghiToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1245, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // aggiornaCollaboratoriToolStripMenuItem
            // 
            this.aggiornaCollaboratoriToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aggiornaCollaboratoriToolStripMenuItem1,
            this.aggiungiCollaboratoriToolStripMenuItem});
            this.aggiornaCollaboratoriToolStripMenuItem.Name = "aggiornaCollaboratoriToolStripMenuItem";
            this.aggiornaCollaboratoriToolStripMenuItem.Size = new System.Drawing.Size(129, 20);
            this.aggiornaCollaboratoriToolStripMenuItem.Text = "Gestisci collaboratori";
            // 
            // aggiornaCollaboratoriToolStripMenuItem1
            // 
            this.aggiornaCollaboratoriToolStripMenuItem1.Name = "aggiornaCollaboratoriToolStripMenuItem1";
            this.aggiornaCollaboratoriToolStripMenuItem1.Size = new System.Drawing.Size(219, 22);
            this.aggiornaCollaboratoriToolStripMenuItem1.Text = "Scarica elenco collaboratori";
            this.aggiornaCollaboratoriToolStripMenuItem1.Click += new System.EventHandler(this.aggiornaCollaboratoriToolStripMenuItem1_Click);
            // 
            // aggiungiCollaboratoriToolStripMenuItem
            // 
            this.aggiungiCollaboratoriToolStripMenuItem.Name = "aggiungiCollaboratoriToolStripMenuItem";
            this.aggiungiCollaboratoriToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.aggiungiCollaboratoriToolStripMenuItem.Text = "Aggiungi collaboratori";
            this.aggiungiCollaboratoriToolStripMenuItem.Click += new System.EventHandler(this.aggiungiCollaboratoriToolStripMenuItem_Click);
            // 
            // gestisciCommesseToolStripMenuItem
            // 
            this.gestisciCommesseToolStripMenuItem.Name = "gestisciCommesseToolStripMenuItem";
            this.gestisciCommesseToolStripMenuItem.Size = new System.Drawing.Size(119, 20);
            this.gestisciCommesseToolStripMenuItem.Text = "Gestisci commesse";
            this.gestisciCommesseToolStripMenuItem.Click += new System.EventHandler(this.gestisciCommesseToolStripMenuItem_Click);
            // 
            // gestisciLuoghiToolStripMenuItem
            // 
            this.gestisciLuoghiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aggiungiLuoghiToolStripMenuItem,
            this.visualizzaTuttoToolStripMenuItem});
            this.gestisciLuoghiToolStripMenuItem.Name = "gestisciLuoghiToolStripMenuItem";
            this.gestisciLuoghiToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.gestisciLuoghiToolStripMenuItem.Text = "Luoghi";
            // 
            // aggiungiLuoghiToolStripMenuItem
            // 
            this.aggiungiLuoghiToolStripMenuItem.Name = "aggiungiLuoghiToolStripMenuItem";
            this.aggiungiLuoghiToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.aggiungiLuoghiToolStripMenuItem.Text = "Aggiungi luoghi";
            this.aggiungiLuoghiToolStripMenuItem.Click += new System.EventHandler(this.aggiungiLuoghiToolStripMenuItem_Click);
            // 
            // visualizzaTuttoToolStripMenuItem
            // 
            this.visualizzaTuttoToolStripMenuItem.Name = "visualizzaTuttoToolStripMenuItem";
            this.visualizzaTuttoToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.visualizzaTuttoToolStripMenuItem.Text = "Visualizza tutto";
            this.visualizzaTuttoToolStripMenuItem.Click += new System.EventHandler(this.visualizzaTuttoToolStripMenuItem_Click);
            // 
            // btnVisualizzaTutti
            // 
            this.btnVisualizzaTutti.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVisualizzaTutti.Location = new System.Drawing.Point(982, 69);
            this.btnVisualizzaTutti.Name = "btnVisualizzaTutti";
            this.btnVisualizzaTutti.Size = new System.Drawing.Size(111, 23);
            this.btnVisualizzaTutti.TabIndex = 9;
            this.btnVisualizzaTutti.Text = "Visualizza tutti";
            this.btnVisualizzaTutti.UseVisualStyleBackColor = true;
            this.btnVisualizzaTutti.Click += new System.EventHandler(this.btnCerca_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(744, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "User";
            // 
            // txtUser
            // 
            this.txtUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUser.Location = new System.Drawing.Point(779, 27);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(100, 20);
            this.txtUser.TabIndex = 11;
            this.txtUser.TextChanged += new System.EventHandler(this.txtUser_TextChanged);
            this.txtUser.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUser_KeyDown);
            // 
            // btnLogin
            // 
            this.btnLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogin.Location = new System.Drawing.Point(1050, 28);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(85, 23);
            this.btnLogin.TabIndex = 13;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtPW
            // 
            this.txtPW.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPW.Location = new System.Drawing.Point(944, 28);
            this.txtPW.Name = "txtPW";
            this.txtPW.PasswordChar = '*';
            this.txtPW.Size = new System.Drawing.Size(100, 20);
            this.txtPW.TabIndex = 14;
            this.txtPW.TextChanged += new System.EventHandler(this.txtPW_TextChanged);
            this.txtPW.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPW_KeyDown);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(885, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Password";
            // 
            // btnLogout
            // 
            this.btnLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogout.Location = new System.Drawing.Point(1141, 28);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(85, 23);
            this.btnLogout.TabIndex = 16;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // frmCollaboratori
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1245, 634);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPW);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnVisualizzaTutti);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.btnElimina);
            this.Controls.Add(this.cmbCognome);
            this.Controls.Add(this.lblcognome);
            this.Controls.Add(this.dgwvisualizza);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCollaboratori";
            this.Text = "Collaboratori";
            this.Load += new System.EventHandler(this.frmCollaboratori_Load);
            this.Click += new System.EventHandler(this.frmCollaboratori_Click);
            ((System.ComponentModel.ISupportInitialize)(this.dgwvisualizza)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgwvisualizza;
        private System.Windows.Forms.ComboBox cmbCognome;
        private System.Windows.Forms.Label lblcognome;
        private System.Windows.Forms.Button btnElimina;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aggiornaCollaboratoriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aggiornaCollaboratoriToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aggiungiCollaboratoriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestisciCommesseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestisciLuoghiToolStripMenuItem;
        private System.Windows.Forms.Button btnVisualizzaTutti;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtPW;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem aggiungiLuoghiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem visualizzaTuttoToolStripMenuItem;
        private System.Windows.Forms.Button btnLogout;
    }
}