﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace Gestionale_Commesse
{
    /// <summary>
    /// Classe per la gestione della tabella ristorante
    /// </summary>
    static class TRistorante
    {
        static string connString = Properties.Settings.ConnectionString;

        /// <summary>
        /// Funzione che salva il contenuto di una lista di risotranti sulla tabella, come parametri la funzione vuole una lista di ristoranti
        /// </summary>
        public static void Save(List<VittoAlloggio> lr)
        {
            string cmdString = "INSERT INTO Ristorante (IDRistorante,IDLuogo,Nome,Indirizzo) VALUES (@IDRistorante,@IDLuogo,@Nome,@Indirizzo)";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                foreach (var item in lr)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = cmdString;
                    cmd.Parameters.AddWithValue("@IDRistorante", item.IDVittoAlloggio);
                    cmd.Parameters.AddWithValue("@IDLuogo", item.IDLuogo);
                    cmd.Parameters.AddWithValue("@Nome", item.Nome);
                    cmd.Parameters.AddWithValue("@Indirizzo", item.Indirizzo);
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        /// <summary>
        /// Funzione che conta le righe del database, ritorna -1 in caso si verifichino degli errori
        /// </summary>
        public static int RowCount()
        {
            try
            {
                string cmdString = "SELECT COUNT(*) FROM Ristorante";
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = cmdString;
                    int n = (int)cmd.ExecuteScalar();
                    conn.Close();
                    return n;
                }
            }
            catch { return -1; }
        }

        public static bool Empty
        { get { return RowCount() <= 0; } }

        /// <summary>
        /// Funzione che legge una lista di tipo Ristorante di un determinato luogo dalla tabella Ristorante, coma parametri vuole l'id del luogo
        /// </summary>
        public static List<VittoAlloggio> Read(string IDLuogo)
        {
            List<VittoAlloggio> l = new List<VittoAlloggio>();
            string cmdString = "SELECT * FROM Ristorante WHERE IDLuogo=@IDLuogo";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                cmd.Parameters.AddWithValue("@IDLuogo", IDLuogo);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(new VittoAlloggio(reader["IDLuogo"].ToString(), reader["Nome"].ToString(), reader["Indirizzo"].ToString()));
                conn.Close();
            }
            return l;
        }

        /// <summary>
        /// Funzione che legge una lista di tipo Ristorante di un determinato luogo dalla tabella Ristorante
        /// </summary>
        public static List<VittoAlloggio> ReadAll()
        {
            List<VittoAlloggio> l = new List<VittoAlloggio>();
            string cmdString = "SELECT * FROM Ristorante";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(new VittoAlloggio(reader["IDLuogo"].ToString(), reader["Nome"].ToString(), reader["Indirizzo"].ToString()));
                conn.Close();
            }
            return l;
        }
        /// <summary>
        /// Funzione che aggiorna la tabella Ristorante aggiungendone nuovi o sovrascrivendoli, come parametri richiede una lista di ristoranti
        /// </summary>
        public static void Update(List<VittoAlloggio> lr)
        {
            string cmdString = "IF EXISTS (SELECT * FROM Ristorante WHERE IDLuogo=@IDLuogo AND IDRistorante = @IDRistorante)" +
                " UPDATE Ristorante SET IDRistorante = @IDRistorante, IDLuogo = @IDLuogo, Nome = @Nome, Indirizzo = @Indirizzo" +
                " WHERE IDLuogo = @IDLuogo AND IDRistorante = @IDRistorante ELSE INSERT INTO Ristorante (IDRistorante, IDLuogo, Nome, Indirizzo) VALUES (@IDRistorante, @IDLuogo, @Nome, @Indirizzo)";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                foreach (var item in lr)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = cmdString;
                    cmd.Parameters.AddWithValue("@IDLuogo", item.IDLuogo);
                    cmd.Parameters.AddWithValue("@Nome", item.Nome);
                    cmd.Parameters.AddWithValue("@Indirizzo", item.Indirizzo);
                    cmd.Parameters.AddWithValue("@IDRistorante", item.IDVittoAlloggio);
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }
    }
}