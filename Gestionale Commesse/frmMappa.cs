﻿using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Gestionale_Commesse
{
    public partial class frmMappa : Form
    {
        List<Luogo> l; // Lista contenente luoghi delle commesse
        List<VittoAlloggio> v = new List<VittoAlloggio>(); // Lista contenente tutti i ristoranti e gli hotel
        public frmMappa(List<Luogo> l)
        {
            InitializeComponent();
            this.l = l;
        }
        public frmMappa(List<Luogo> l, List<VittoAlloggio> r, List<VittoAlloggio> h)
        {
            InitializeComponent();
            this.l = l;
            v.AddRange(r);
            v.AddRange(h);
        }

        /// <summary>
        /// Visualizza i luoghi presenti nella lista nella mappa
        /// </summary>
        private void frmMappa_Load(object sender, EventArgs e)
        {
            gMapControl1.MapProvider = GMap.NET.MapProviders.GoogleMapProvider.Instance;
            GMaps.Instance.Mode = AccessMode.ServerAndCache;
            gMapControl1.DragButton = MouseButtons.Left;
            gMapControl1.ShowCenter = false;
            gMapControl1.MinZoom = 3;
            gMapControl1.MaxZoom = 20;
            foreach (var item in l)
            {
                gMapControl1.SetPositionByKeywords(item.Indirizzo + ',' + item.Citta + ',' + item.Nazione);
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(gMapControl1.Position.Lat, gMapControl1.Position.Lng), GMarkerGoogleType.orange);
                markersOverlay.Markers.Add(marker);
                gMapControl1.Overlays.Add(markersOverlay);
                marker.ToolTipText = IDCommessa(item.IDLuogo);
            }

            foreach (var item in v)
            {
                Luogo luogo = TLuogo.Cerca(item.IDLuogo)[0];
                gMapControl1.SetPositionByKeywords(item.Indirizzo + ',' + luogo.Citta + ',' + luogo.Nazione);
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(gMapControl1.Position.Lat, gMapControl1.Position.Lng), GMarkerGoogleType.blue);
                markersOverlay.Markers.Add(marker);
                gMapControl1.Overlays.Add(markersOverlay);
            }

            gMapControl1.Zoom = 9;

        }
        private string IDCommessa(string s)
        {
            string ids = "";
            List<Commessa> l = TCommessa.TrovaLuogo(s);
            foreach (var item in l)
                ids += item.IDCommessa + "\n";
            return ids;
        }

        private void gMapControl1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                Cursor = Cursors.Hand;
            else
                Cursor = Cursors.Default;
        }
    }
}
