﻿namespace Gestionale_Commesse
{
    partial class frmGestisciCommessa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGestisciCommessa));
            this.lblcodice = new System.Windows.Forms.Label();
            this.lblData = new System.Windows.Forms.Label();
            this.lblLuoghi = new System.Windows.Forms.Label();
            this.lblcliente = new System.Windows.Forms.Label();
            this.txtcliente = new System.Windows.Forms.TextBox();
            this.cmbDestinazione = new System.Windows.Forms.ComboBox();
            this.btnNuova = new System.Windows.Forms.Button();
            this.dtpdata = new System.Windows.Forms.DateTimePicker();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm6 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dgwcollaboratori = new System.Windows.Forms.DataGridView();
            this.clm1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm4 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.IdCl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbcollaboratore = new System.Windows.Forms.ComboBox();
            this.btnElimCollab = new System.Windows.Forms.Button();
            this.btnDettagli = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAggiungiDestinazione = new System.Windows.Forms.Button();
            this.cmbCodiceCommessa = new System.Windows.Forms.ComboBox();
            this.btnEliminaCommessa = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnChiudiVittoAlloggio = new System.Windows.Forms.Button();
            this.dgvVittoAlloggio = new System.Windows.Forms.DataGridView();
            this.IdVittoAlloggio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdDestinazione = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnMappa = new System.Windows.Forms.Button();
            this.rdoHotel = new System.Windows.Forms.RadioButton();
            this.RdoRistoranti = new System.Windows.Forms.RadioButton();
            this.btnChiudiVisDestinazioni = new System.Windows.Forms.Button();
            this.lbLuogo = new System.Windows.Forms.Label();
            this.cmbluogo = new System.Windows.Forms.ComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.visualizzaTutteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mappaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vittoAlloggioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visualizzaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inserisciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dgwluogo = new System.Windows.Forms.DataGridView();
            this.IDLuogo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nazione = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Citta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Indirizzo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Moneta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Lingua = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnCloseCommesseCollaboratore = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgwcollaboratori)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVittoAlloggio)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwluogo)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblcodice
            // 
            this.lblcodice.AutoSize = true;
            this.lblcodice.Location = new System.Drawing.Point(23, 16);
            this.lblcodice.Name = "lblcodice";
            this.lblcodice.Size = new System.Drawing.Size(94, 13);
            this.lblcodice.TabIndex = 0;
            this.lblcodice.Text = "Codice Commessa";
            // 
            // lblData
            // 
            this.lblData.AccessibleDescription = "";
            this.lblData.AutoSize = true;
            this.lblData.Location = new System.Drawing.Point(23, 269);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(30, 13);
            this.lblData.TabIndex = 5;
            this.lblData.Text = "Data";
            // 
            // lblLuoghi
            // 
            this.lblLuoghi.AutoSize = true;
            this.lblLuoghi.Location = new System.Drawing.Point(37, 16);
            this.lblLuoghi.Name = "lblLuoghi";
            this.lblLuoghi.Size = new System.Drawing.Size(104, 13);
            this.lblLuoghi.TabIndex = 0;
            this.lblLuoghi.Text = "Codice Destinazione";
            // 
            // lblcliente
            // 
            this.lblcliente.AutoSize = true;
            this.lblcliente.Location = new System.Drawing.Point(23, 78);
            this.lblcliente.Name = "lblcliente";
            this.lblcliente.Size = new System.Drawing.Size(39, 13);
            this.lblcliente.TabIndex = 2;
            this.lblcliente.Text = "Cliente";
            // 
            // txtcliente
            // 
            this.txtcliente.Location = new System.Drawing.Point(26, 94);
            this.txtcliente.Name = "txtcliente";
            this.txtcliente.Size = new System.Drawing.Size(180, 20);
            this.txtcliente.TabIndex = 3;
            this.txtcliente.TextChanged += new System.EventHandler(this.txtcliente_TextChanged);
            // 
            // cmbDestinazione
            // 
            this.cmbDestinazione.FormattingEnabled = true;
            this.cmbDestinazione.Location = new System.Drawing.Point(6, 32);
            this.cmbDestinazione.Name = "cmbDestinazione";
            this.cmbDestinazione.Size = new System.Drawing.Size(165, 21);
            this.cmbDestinazione.Sorted = true;
            this.cmbDestinazione.TabIndex = 1;
            this.cmbDestinazione.SelectedIndexChanged += new System.EventHandler(this.cmbDestinazione_SelectedIndexChanged);
            this.cmbDestinazione.TextChanged += new System.EventHandler(this.cmbDestinazione_TextChanged);
            // 
            // btnNuova
            // 
            this.btnNuova.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNuova.Location = new System.Drawing.Point(704, 409);
            this.btnNuova.Name = "btnNuova";
            this.btnNuova.Size = new System.Drawing.Size(118, 25);
            this.btnNuova.TabIndex = 9;
            this.btnNuova.Text = "Nuova commessa";
            this.btnNuova.UseVisualStyleBackColor = true;
            this.btnNuova.Click += new System.EventHandler(this.btnNuova_Click);
            // 
            // dtpdata
            // 
            this.dtpdata.Location = new System.Drawing.Point(26, 285);
            this.dtpdata.Name = "dtpdata";
            this.dtpdata.Size = new System.Drawing.Size(180, 20);
            this.dtpdata.TabIndex = 6;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.clm5,
            this.clm6});
            this.dataGridView1.Location = new System.Drawing.Point(6, 8);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(601, 106);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ID Commessa";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Cliente";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // clm5
            // 
            this.clm5.HeaderText = "Data Consegna";
            this.clm5.Name = "clm5";
            this.clm5.ReadOnly = true;
            // 
            // clm6
            // 
            this.clm6.HeaderText = "ID Luogo";
            this.clm6.Name = "clm6";
            this.clm6.ReadOnly = true;
            // 
            // dgwcollaboratori
            // 
            this.dgwcollaboratori.AllowUserToAddRows = false;
            this.dgwcollaboratori.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgwcollaboratori.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgwcollaboratori.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwcollaboratori.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clm1,
            this.clm2,
            this.clm3,
            this.clm4,
            this.IdCl});
            this.dgwcollaboratori.Location = new System.Drawing.Point(6, 56);
            this.dgwcollaboratori.MultiSelect = false;
            this.dgwcollaboratori.Name = "dgwcollaboratori";
            this.dgwcollaboratori.ReadOnly = true;
            this.dgwcollaboratori.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgwcollaboratori.Size = new System.Drawing.Size(601, 323);
            this.dgwcollaboratori.TabIndex = 5;
            this.dgwcollaboratori.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgwcollaboratori_CellContentClick);
            this.dgwcollaboratori.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgwcollaboratori_CellValueChanged);
            this.dgwcollaboratori.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgwcollaboratori_RowsAdded);
            this.dgwcollaboratori.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgwcollaboratori_RowsRemoved);
            // 
            // clm1
            // 
            this.clm1.HeaderText = "Cognome";
            this.clm1.Name = "clm1";
            this.clm1.ReadOnly = true;
            // 
            // clm2
            // 
            this.clm2.HeaderText = "Nome";
            this.clm2.Name = "clm2";
            this.clm2.ReadOnly = true;
            // 
            // clm3
            // 
            this.clm3.HeaderText = "Reparto";
            this.clm3.Name = "clm3";
            this.clm3.ReadOnly = true;
            // 
            // clm4
            // 
            this.clm4.HeaderText = "Stato";
            this.clm4.Name = "clm4";
            this.clm4.ReadOnly = true;
            // 
            // IdCl
            // 
            this.IdCl.HeaderText = "Id collaboratore";
            this.IdCl.Name = "IdCl";
            this.IdCl.ReadOnly = true;
            // 
            // cmbcollaboratore
            // 
            this.cmbcollaboratore.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbcollaboratore.FormattingEnabled = true;
            this.cmbcollaboratore.Location = new System.Drawing.Point(6, 28);
            this.cmbcollaboratore.Name = "cmbcollaboratore";
            this.cmbcollaboratore.Size = new System.Drawing.Size(482, 21);
            this.cmbcollaboratore.TabIndex = 4;
            this.cmbcollaboratore.SelectedIndexChanged += new System.EventHandler(this.cmbcollaboratore_SelectedIndexChanged);
            // 
            // btnElimCollab
            // 
            this.btnElimCollab.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnElimCollab.Location = new System.Drawing.Point(494, 25);
            this.btnElimCollab.Name = "btnElimCollab";
            this.btnElimCollab.Size = new System.Drawing.Size(113, 25);
            this.btnElimCollab.TabIndex = 2;
            this.btnElimCollab.Text = "Rimuovi";
            this.btnElimCollab.UseVisualStyleBackColor = true;
            this.btnElimCollab.Click += new System.EventHandler(this.btnElimCollab_Click);
            // 
            // btnDettagli
            // 
            this.btnDettagli.Location = new System.Drawing.Point(30, 88);
            this.btnDettagli.Name = "btnDettagli";
            this.btnDettagli.Size = new System.Drawing.Size(122, 22);
            this.btnDettagli.TabIndex = 3;
            this.btnDettagli.Text = "Visualizza dettagli";
            this.btnDettagli.UseVisualStyleBackColor = true;
            this.btnDettagli.Click += new System.EventHandler(this.btnDettagli_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAggiungiDestinazione);
            this.groupBox1.Controls.Add(this.cmbDestinazione);
            this.groupBox1.Controls.Add(this.lblLuoghi);
            this.groupBox1.Controls.Add(this.btnDettagli);
            this.groupBox1.Location = new System.Drawing.Point(26, 141);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(180, 119);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // btnAggiungiDestinazione
            // 
            this.btnAggiungiDestinazione.Location = new System.Drawing.Point(30, 59);
            this.btnAggiungiDestinazione.Name = "btnAggiungiDestinazione";
            this.btnAggiungiDestinazione.Size = new System.Drawing.Size(122, 23);
            this.btnAggiungiDestinazione.TabIndex = 2;
            this.btnAggiungiDestinazione.Text = "Aggiungi destinazione";
            this.btnAggiungiDestinazione.UseVisualStyleBackColor = true;
            this.btnAggiungiDestinazione.Click += new System.EventHandler(this.btnAggiungiDestinazione_Click);
            // 
            // cmbCodiceCommessa
            // 
            this.cmbCodiceCommessa.FormattingEnabled = true;
            this.cmbCodiceCommessa.Location = new System.Drawing.Point(26, 32);
            this.cmbCodiceCommessa.Name = "cmbCodiceCommessa";
            this.cmbCodiceCommessa.Size = new System.Drawing.Size(180, 21);
            this.cmbCodiceCommessa.TabIndex = 1;
            this.cmbCodiceCommessa.SelectedIndexChanged += new System.EventHandler(this.cmbCodiceCommessa_SelectedIndexChanged);
            this.cmbCodiceCommessa.TextChanged += new System.EventHandler(this.cmbCodiceCommessa_TextChanged);
            // 
            // btnEliminaCommessa
            // 
            this.btnEliminaCommessa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEliminaCommessa.Location = new System.Drawing.Point(579, 409);
            this.btnEliminaCommessa.Name = "btnEliminaCommessa";
            this.btnEliminaCommessa.Size = new System.Drawing.Size(119, 25);
            this.btnEliminaCommessa.TabIndex = 8;
            this.btnEliminaCommessa.Text = "Elimina commessa";
            this.btnEliminaCommessa.UseVisualStyleBackColor = true;
            this.btnEliminaCommessa.Click += new System.EventHandler(this.btnEliminaCommessa_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btnChiudiVisDestinazioni);
            this.panel1.Controls.Add(this.lbLuogo);
            this.panel1.Controls.Add(this.cmbluogo);
            this.panel1.Controls.Add(this.menuStrip1);
            this.panel1.Controls.Add(this.dgwluogo);
            this.panel1.Location = new System.Drawing.Point(210, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(613, 385);
            this.panel1.TabIndex = 12;
            this.panel1.VisibleChanged += new System.EventHandler(this.panel1_VisibleChanged);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.btnChiudiVittoAlloggio);
            this.panel2.Controls.Add(this.dgvVittoAlloggio);
            this.panel2.Controls.Add(this.btnMappa);
            this.panel2.Controls.Add(this.rdoHotel);
            this.panel2.Controls.Add(this.RdoRistoranti);
            this.panel2.Location = new System.Drawing.Point(0, 213);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(613, 172);
            this.panel2.TabIndex = 4;
            // 
            // btnChiudiVittoAlloggio
            // 
            this.btnChiudiVittoAlloggio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChiudiVittoAlloggio.Location = new System.Drawing.Point(522, 138);
            this.btnChiudiVittoAlloggio.Name = "btnChiudiVittoAlloggio";
            this.btnChiudiVittoAlloggio.Size = new System.Drawing.Size(76, 23);
            this.btnChiudiVittoAlloggio.TabIndex = 4;
            this.btnChiudiVittoAlloggio.Text = "Chiudi";
            this.btnChiudiVittoAlloggio.UseVisualStyleBackColor = true;
            this.btnChiudiVittoAlloggio.Click += new System.EventHandler(this.btnChiudiVittoAlloggio_Click);
            // 
            // dgvVittoAlloggio
            // 
            this.dgvVittoAlloggio.AllowUserToAddRows = false;
            this.dgvVittoAlloggio.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvVittoAlloggio.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvVittoAlloggio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVittoAlloggio.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdVittoAlloggio,
            this.IdDestinazione,
            this.Nome,
            this.dataGridViewTextBoxColumn3});
            this.dgvVittoAlloggio.Location = new System.Drawing.Point(12, 33);
            this.dgvVittoAlloggio.Name = "dgvVittoAlloggio";
            this.dgvVittoAlloggio.ReadOnly = true;
            this.dgvVittoAlloggio.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVittoAlloggio.Size = new System.Drawing.Size(592, 99);
            this.dgvVittoAlloggio.TabIndex = 2;
            this.dgvVittoAlloggio.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVittoAlloggio_CellValueChanged_1);
            // 
            // IdVittoAlloggio
            // 
            this.IdVittoAlloggio.HeaderText = "IdVittoAlloggio";
            this.IdVittoAlloggio.Name = "IdVittoAlloggio";
            this.IdVittoAlloggio.ReadOnly = true;
            // 
            // IdDestinazione
            // 
            this.IdDestinazione.HeaderText = "IdDestinazione";
            this.IdDestinazione.Name = "IdDestinazione";
            this.IdDestinazione.ReadOnly = true;
            // 
            // Nome
            // 
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Indirizzo";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // btnMappa
            // 
            this.btnMappa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMappa.Location = new System.Drawing.Point(396, 138);
            this.btnMappa.Name = "btnMappa";
            this.btnMappa.Size = new System.Drawing.Size(120, 23);
            this.btnMappa.TabIndex = 3;
            this.btnMappa.Text = "Mostra sulla mappa";
            this.btnMappa.UseVisualStyleBackColor = true;
            this.btnMappa.Click += new System.EventHandler(this.btnMappa_Click_1);
            // 
            // rdoHotel
            // 
            this.rdoHotel.AutoSize = true;
            this.rdoHotel.Location = new System.Drawing.Point(87, 10);
            this.rdoHotel.Name = "rdoHotel";
            this.rdoHotel.Size = new System.Drawing.Size(50, 17);
            this.rdoHotel.TabIndex = 1;
            this.rdoHotel.TabStop = true;
            this.rdoHotel.Text = "Hotel";
            this.rdoHotel.UseVisualStyleBackColor = true;
            this.rdoHotel.CheckedChanged += new System.EventHandler(this.rdoHotel_CheckedChanged_1);
            // 
            // RdoRistoranti
            // 
            this.RdoRistoranti.AutoSize = true;
            this.RdoRistoranti.Location = new System.Drawing.Point(12, 10);
            this.RdoRistoranti.Name = "RdoRistoranti";
            this.RdoRistoranti.Size = new System.Drawing.Size(69, 17);
            this.RdoRistoranti.TabIndex = 0;
            this.RdoRistoranti.TabStop = true;
            this.RdoRistoranti.Text = "Ristoranti";
            this.RdoRistoranti.UseVisualStyleBackColor = true;
            this.RdoRistoranti.CheckedChanged += new System.EventHandler(this.RdoRistoranti_CheckedChanged_1);
            // 
            // btnChiudiVisDestinazioni
            // 
            this.btnChiudiVisDestinazioni.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChiudiVisDestinazioni.Location = new System.Drawing.Point(528, 359);
            this.btnChiudiVisDestinazioni.Name = "btnChiudiVisDestinazioni";
            this.btnChiudiVisDestinazioni.Size = new System.Drawing.Size(76, 23);
            this.btnChiudiVisDestinazioni.TabIndex = 5;
            this.btnChiudiVisDestinazioni.Text = "Chiudi";
            this.btnChiudiVisDestinazioni.UseVisualStyleBackColor = true;
            this.btnChiudiVisDestinazioni.Click += new System.EventHandler(this.btnChiudiVisDestinazioni_Click);
            // 
            // lbLuogo
            // 
            this.lbLuogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbLuogo.AutoSize = true;
            this.lbLuogo.Location = new System.Drawing.Point(364, 12);
            this.lbLuogo.Name = "lbLuogo";
            this.lbLuogo.Size = new System.Drawing.Size(68, 13);
            this.lbLuogo.TabIndex = 1;
            this.lbLuogo.Text = "Destinazione";
            // 
            // cmbluogo
            // 
            this.cmbluogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbluogo.FormattingEnabled = true;
            this.cmbluogo.Location = new System.Drawing.Point(438, 9);
            this.cmbluogo.Name = "cmbluogo";
            this.cmbluogo.Size = new System.Drawing.Size(160, 21);
            this.cmbluogo.TabIndex = 2;
            this.cmbluogo.SelectedIndexChanged += new System.EventHandler(this.cmbluogo_SelectedIndexChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.visualizzaTutteToolStripMenuItem,
            this.mappaToolStripMenuItem,
            this.vittoAlloggioToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(12, 6);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(251, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // visualizzaTutteToolStripMenuItem
            // 
            this.visualizzaTutteToolStripMenuItem.Name = "visualizzaTutteToolStripMenuItem";
            this.visualizzaTutteToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
            this.visualizzaTutteToolStripMenuItem.Text = "Visualizza tutte";
            this.visualizzaTutteToolStripMenuItem.Click += new System.EventHandler(this.visualizzaTutteToolStripMenuItem_Click);
            // 
            // mappaToolStripMenuItem
            // 
            this.mappaToolStripMenuItem.Name = "mappaToolStripMenuItem";
            this.mappaToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.mappaToolStripMenuItem.Text = "Mappa";
            this.mappaToolStripMenuItem.Click += new System.EventHandler(this.mappaToolStripMenuItem_Click_1);
            // 
            // vittoAlloggioToolStripMenuItem
            // 
            this.vittoAlloggioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.visualizzaToolStripMenuItem,
            this.inserisciToolStripMenuItem});
            this.vittoAlloggioToolStripMenuItem.Name = "vittoAlloggioToolStripMenuItem";
            this.vittoAlloggioToolStripMenuItem.Size = new System.Drawing.Size(90, 20);
            this.vittoAlloggioToolStripMenuItem.Text = "Vitto alloggio";
            // 
            // visualizzaToolStripMenuItem
            // 
            this.visualizzaToolStripMenuItem.Name = "visualizzaToolStripMenuItem";
            this.visualizzaToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.visualizzaToolStripMenuItem.Text = "Visualizza";
            this.visualizzaToolStripMenuItem.Click += new System.EventHandler(this.visualizzaToolStripMenuItem_Click);
            // 
            // inserisciToolStripMenuItem
            // 
            this.inserisciToolStripMenuItem.Name = "inserisciToolStripMenuItem";
            this.inserisciToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.inserisciToolStripMenuItem.Text = "Inserisci";
            this.inserisciToolStripMenuItem.Click += new System.EventHandler(this.inserisciToolStripMenuItem_Click);
            // 
            // dgwluogo
            // 
            this.dgwluogo.AllowUserToAddRows = false;
            this.dgwluogo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgwluogo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgwluogo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwluogo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDLuogo,
            this.Nazione,
            this.Citta,
            this.Indirizzo,
            this.Moneta,
            this.Lingua});
            this.dgwluogo.Location = new System.Drawing.Point(12, 36);
            this.dgwluogo.MultiSelect = false;
            this.dgwluogo.Name = "dgwluogo";
            this.dgwluogo.ReadOnly = true;
            this.dgwluogo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgwluogo.Size = new System.Drawing.Size(592, 317);
            this.dgwluogo.TabIndex = 3;
            this.dgwluogo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgwluogo_CellClick);
            // 
            // IDLuogo
            // 
            this.IDLuogo.HeaderText = "IDLuogo";
            this.IDLuogo.Name = "IDLuogo";
            this.IDLuogo.ReadOnly = true;
            // 
            // Nazione
            // 
            this.Nazione.HeaderText = "Nazione";
            this.Nazione.Name = "Nazione";
            this.Nazione.ReadOnly = true;
            // 
            // Citta
            // 
            this.Citta.HeaderText = "Città";
            this.Citta.Name = "Citta";
            this.Citta.ReadOnly = true;
            // 
            // Indirizzo
            // 
            this.Indirizzo.HeaderText = "Indirizzo";
            this.Indirizzo.Name = "Indirizzo";
            this.Indirizzo.ReadOnly = true;
            // 
            // Moneta
            // 
            this.Moneta.HeaderText = "Moneta";
            this.Moneta.Name = "Moneta";
            this.Moneta.ReadOnly = true;
            // 
            // Lingua
            // 
            this.Lingua.HeaderText = "Lingua";
            this.Lingua.Name = "Lingua";
            this.Lingua.ReadOnly = true;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.Controls.Add(this.panel3);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.dgwcollaboratori);
            this.panel4.Controls.Add(this.cmbcollaboratore);
            this.panel4.Controls.Add(this.btnElimCollab);
            this.panel4.Location = new System.Drawing.Point(210, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(613, 385);
            this.panel4.TabIndex = 7;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.btnCloseCommesseCollaboratore);
            this.panel3.Controls.Add(this.dataGridView1);
            this.panel3.Location = new System.Drawing.Point(0, 239);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(613, 146);
            this.panel3.TabIndex = 0;
            // 
            // btnCloseCommesseCollaboratore
            // 
            this.btnCloseCommesseCollaboratore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseCommesseCollaboratore.Location = new System.Drawing.Point(532, 117);
            this.btnCloseCommesseCollaboratore.Name = "btnCloseCommesseCollaboratore";
            this.btnCloseCommesseCollaboratore.Size = new System.Drawing.Size(75, 23);
            this.btnCloseCommesseCollaboratore.TabIndex = 1;
            this.btnCloseCommesseCollaboratore.Text = "Chiudi";
            this.btnCloseCommesseCollaboratore.UseVisualStyleBackColor = true;
            this.btnCloseCommesseCollaboratore.Click += new System.EventHandler(this.btnCloseCommesseCollaboratore_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Collaboratori";
            // 
            // frmGestisciCommessa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 446);
            this.Controls.Add(this.cmbCodiceCommessa);
            this.Controls.Add(this.btnEliminaCommessa);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dtpdata);
            this.Controls.Add(this.btnNuova);
            this.Controls.Add(this.txtcliente);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.lblcliente);
            this.Controls.Add(this.lblData);
            this.Controls.Add(this.lblcodice);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmGestisciCommessa";
            this.Text = "Aggiungi commessa";
            this.Load += new System.EventHandler(this.frmAggiungiCommessa_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgwcollaboratori)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVittoAlloggio)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwluogo)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblcodice;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.Label lblLuoghi;
        private System.Windows.Forms.Label lblcliente;
        private System.Windows.Forms.TextBox txtcliente;
        private System.Windows.Forms.ComboBox cmbDestinazione;
        private System.Windows.Forms.Button btnNuova;
        private System.Windows.Forms.DateTimePicker dtpdata;
        private System.Windows.Forms.DataGridView dgwcollaboratori;
        private System.Windows.Forms.Button btnDettagli;
        private System.Windows.Forms.Button btnElimCollab;
        private System.Windows.Forms.ComboBox cmbcollaboratore;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm1;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm2;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm3;
        private System.Windows.Forms.DataGridViewButtonColumn clm4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbCodiceCommessa;
        private System.Windows.Forms.Button btnEliminaCommessa;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdCl;
        private System.Windows.Forms.Button btnAggiungiDestinazione;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgwluogo;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDLuogo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nazione;
        private System.Windows.Forms.DataGridViewTextBoxColumn Citta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Indirizzo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Moneta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lingua;
        private System.Windows.Forms.Label lbLuogo;
        private System.Windows.Forms.ComboBox cmbluogo;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem visualizzaTutteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mappaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vittoAlloggioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem visualizzaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inserisciToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm5;
        private System.Windows.Forms.DataGridViewButtonColumn clm6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rdoHotel;
        private System.Windows.Forms.RadioButton RdoRistoranti;
        private System.Windows.Forms.DataGridView dgvVittoAlloggio;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdVittoAlloggio;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDestinazione;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Button btnMappa;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnCloseCommesseCollaboratore;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnChiudiVittoAlloggio;
        private System.Windows.Forms.Button btnChiudiVisDestinazioni;
    }
}