﻿using Supremes;
using Supremes.Nodes;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;

namespace Gestionale_Commesse
{
    /// <summary>
    /// Classe contenente tutte le funzioni per gestire la tabella collaboratore
    /// </summary>
    static class TCollaboratore
    {
        static string connString = Properties.Settings.ConnectionString;

        /// <summary>
        ///  Funzione che aggiorna più righe della tabella collaboratore, come parametri vuole una lista di tipo collaboratore 
        /// </summary>
        public static void Update(List<Collaboratore> lc)
        {
            string cmdString = "IF EXISTS (SELECT * FROM Collaboratore WHERE Id_cl=@Id_cl)" +
                " UPDATE Collaboratore SET Nome = @Nome, Cognome = @Cognome, Reparto = @Reparto, Email = @Email, TelInterno = @TelInterno, Cellulare = @Cellulare, Tipocoll = @Tipocoll, DataAss = @DataAss, Comune = @Comune" +
                " WHERE Id_cl = @Id_cl ELSE INSERT INTO Collaboratore (Nome, Cognome, Reparto, Email, TelInterno, Cellulare, Tipocoll, DataAss, Comune, Id_cl) VALUES (@Nome, @Cognome, @Reparto, @Email, @TelInterno, @Cellulare, @TipoColl, @DataAss, @Comune, @Id_cl)";
            SqlConnection conn = new SqlConnection(connString);
            conn.Open();
            foreach (var c in lc)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                cmd.Parameters.AddWithValue("@Nome", c.Nome);
                cmd.Parameters.AddWithValue("@Cognome", c.Cognome);
                cmd.Parameters.AddWithValue("@Reparto", c.Reparto);
                cmd.Parameters.AddWithValue("@Email", c.Email);
                cmd.Parameters.AddWithValue("@TelInterno", c.TelInterno);
                cmd.Parameters.AddWithValue("@Cellulare", c.Cellulare);
                cmd.Parameters.AddWithValue("@Tipocoll", c.TipoDiCollaborazione);
                cmd.Parameters.AddWithValue("@DataAss", c.DataAssunzione);
                cmd.Parameters.AddWithValue("@Comune", c.Comune);
                cmd.Parameters.AddWithValue("@Id_cl", c.IDCollaboratore);
                cmd.ExecuteNonQuery();
            }
            conn.Close();
        }

        /// <summary>
        /// Funzione che conta il numero delle righe della tabella, non vuole nessun parametro
        /// ritorna il numero di righe se non si verifica nessun problema, ritorna -1 se si verifica un eccezione
        /// </summary>
        public static int RowCount()
        {
            try
            {
                string cmdString = "SELECT COUNT (*) FROM Collaboratore";
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                int n = (int)cmd.ExecuteScalar();
                conn.Close();
                return n;
            }
            catch { return -1; };
        }

        /// <summary>
        /// Funzione che legge tutti i dati della tabella collaboratore, ritorna una lista di collaboratori
        /// </summary>
        public static List<Collaboratore> Read()
        {
            List<Collaboratore> l = new List<Collaboratore>();
            string cmdString = "SELECT * FROM Collaboratore";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(new Collaboratore(reader["Nome"].ToString(), reader["Cognome"].ToString(), reader["Reparto"].ToString(), reader["Email"].ToString(), reader["TelInterno"].ToString(), reader["Cellulare"].ToString(), reader["TipoColl"].ToString(), reader["DataAss"].ToString(), reader["Comune"].ToString(), reader["Id_cl"].ToString()));
                conn.Close();
                return l;
            }
        }

        public static bool Empty
        { get { return RowCount() <= 0; } }

        /// <summary>
        /// Funzione che cerca un collaboratore in base al cognome, come parametri vuole una stringa contentente il cognome del collaboratore
        /// ritorna una lista di collaboratori
        /// </summary>
        public static List<Collaboratore> Cerca(string cognome)
        {
            List<Collaboratore> l = new List<Collaboratore>();
            string cmdString = "SELECT * FROM Collaboratore WHERE cognome LIKE @cognome";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.AddWithValue("@cognome", "%" + cognome + "%");
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(new Collaboratore(reader["Nome"].ToString(), reader["Cognome"].ToString(), reader["Reparto"].ToString(), reader["Email"].ToString(), reader["TelInterno"].ToString(), reader["Cellulare"].ToString(), reader["TipoColl"].ToString(), reader["DataAss"].ToString(), reader["Comune"].ToString(), reader["Id_cl"].ToString()));
                conn.Close();
                return l;
            }
        }

        /// <summary>
        /// Funzione che cerca un collaboratore in base al suo ID, come parametri vuole una stringa contentente l'ID del collaboratore, ritorna una lista di collaboratori
        /// </summary>
        public static List<Collaboratore> CercaID(string IDCollaboratore)
        {
            List<Collaboratore> l = new List<Collaboratore>();
            string cmdString = "SELECT * FROM Collaboratore WHERE Id_cl = @IDCollaboratore";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.AddWithValue("@IDCollaboratore", IDCollaboratore);
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(new Collaboratore(reader["Nome"].ToString(), reader["Cognome"].ToString(), reader["Reparto"].ToString(),
                        reader["Email"].ToString(), reader["TelInterno"].ToString(), reader["Cellulare"].ToString(),
                        reader["TipoColl"].ToString(), reader["DataAss"].ToString(), reader["Comune"].ToString(), reader["Id_cl"].ToString()));
                conn.Close();
                return l;
            }

        }

        /// <summary>
        /// Funzione che cerca un collaboratore in base al nominativo (Cognome e nome), come parametri vuole una stringa contentente il cognome 
        /// del collaboratore, ritorna una lista di collaboratori
        /// </summary>
        public static List<Collaboratore> Cerca_Nominativo(string nominativo)
        {
            List<Collaboratore> l = new List<Collaboratore>();
            string cmdString = "SELECT * FROM Collaboratore WHERE Concat(Cognome,' ',Nome) LIKE @Nominativo";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.AddWithValue("@Nominativo", nominativo);
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(new Collaboratore(reader["Nome"].ToString(), reader["Cognome"].ToString(), reader["Reparto"].ToString(), reader["Email"].ToString(), reader["TelInterno"].ToString(), reader["Cellulare"].ToString(), reader["TipoColl"].ToString(), reader["DataAss"].ToString(), reader["Comune"].ToString(), reader["Id_cl"].ToString()));
                conn.Close();
                return l;
            }
        }

        /// <summary>
        /// Funzione che ritorna una lista stringhe rappresentante il cognome dei collaboratori,
        /// </summary>
        public static List<string> Cognomi()
        {
            List<string> l = new List<string>();
            string cmdString = "SELECT cognome FROM Collaboratore";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(reader["cognome"].ToString());
                conn.Close();
                return l;
            }
        }

        /// <summary>
        /// Funzione che ritorna una lista di stringhe rappresentante i reparti dei collaboratori,
        /// </summary>
        public static List<string> Reparto()
        {
            List<string> l = new List<string>();
            string cmdString = "SELECT DISTINCT Reparto FROM Collaboratore";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(reader["Reparto"].ToString());
                conn.Close();
                return l;
            }
        }

        /// <summary>
        /// Funzione che ritorna una lista di stringhe contenente i tipi di collaborazione, come parametro
        /// </summary>
        public static List<string> TipoCollaborazione()
        {
            List<string> l = new List<string>();
            string cmdString = "SELECT DISTINCT TipoColl FROM Collaboratore";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(reader["TipoColl"].ToString());
                conn.Close();
                return l;
            }
        }

        /// <summary>
        /// Funzione che aggiorna il tipo di collaborazione per gli ex collaboratori, come parametri vuole un ID di un collaboratore 
        /// </summary>
        public static void CessataCollaborazione(string IDcollaboratore)
        {
            string cmdString = "UPDATE Collaboratore SET Tipocoll = @Tipocoll WHERE Id_cl = @Id_cl";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.AddWithValue("@Tipocoll", "Cessata");
                cmd.Parameters.AddWithValue("@Id_cl", IDcollaboratore);
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        /// <summary>
        /// Funzione che aggiorna il tipo di collaborazione per gli ex collaboratori, come parametri vuole una lista di collaboratori
        /// </summary>
        public static void CessataCollaborazione(List<Collaboratore> lc)
        {
            string cmdString = "UPDATE Collaboratore SET Tipocoll = @Tipocoll WHERE Id_cl = @Id_cl";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                foreach (var item in lc)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Parameters.AddWithValue("@Tipocoll", "Cessata");
                    cmd.Parameters.AddWithValue("@Id_cl", item.IDCollaboratore);
                    cmd.Connection = conn;
                    cmd.CommandText = cmdString;
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        /// <summary>
        /// Funzione che confronta i collaboratori presenti in DB con quelli dal sito: aggiorna i collaboratori
        /// già presenti in DB; inserisce i nuovi collaboratori ed infine fa cessare la collaborazione se il collaboratore
        /// non è più presente nel sito
        /// </summary>
        public static void UpdateAvanzato(List<Collaboratore> l/*Lista scaricata*/)
        {
            Update(l);
            List<Collaboratore> lc = Read(); //Lista Collaboratori DataBase
            List<Collaboratore> lc1 = new List<Collaboratore>(lc);

            foreach (var item in lc)
                if (l.Contains(item))
                    lc1.Remove(item);

            CessataCollaborazione(lc1);// Aggiorno la tabella Collaboratori con la collaborazione cessata
        }

        /// <summary>
        /// Funzione che ritorna una lista di stringhe contenente i nominativi dei collaboratori (Cognome e Nome),
        /// </summary>
        public static List<string> Nominativi()
        {
            List<string> l = new List<string>();
            string cmdString = "SELECT cognome, nome FROM Collaboratore";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = cmdString;
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    l.Add(reader["Cognome"].ToString() + " " + reader["Nome"].ToString());
                conn.Close();
                return l;
            }
        }

        /// <summary>
        /// Procedura di scaricamento dei collaboratori dal sito della Loccioni, richiama la form Login per ottenere le credenziali,
        /// restituisce una lista di collaboratori
        /// </summary>
        public static List<Collaboratore> DownloadCollaboratori(string user, string pw)
        {
            List<Collaboratore> l = new List<Collaboratore>();

            string contents;
            using (var wc = new WebClient())
            {
                wc.UseDefaultCredentials = true;
                wc.Credentials = new NetworkCredential(user, pw);
                contents = wc.DownloadString("http://intranet.loccioni.com/WebHr/RubricaCollaboratori.aspx");
            }
            Elements el = Dcsoup.Parse(contents).Select("table#GridCollaboratori");
            contents = el.ToString();
            el = el.Select("tr");
            Elements dati = el.ElementAt(0).Select("th");
            dati.RemoveAt(0);
            foreach (Element e in el.Skip(1))
            {
                dati = e.Select("td");
                dati.RemoveAt(0);
                l.Add(new Collaboratore(dati.ElementAt(1).Text, dati.ElementAt(0).Text, dati.ElementAt(2).Text, dati.ElementAt(3).Text, dati.ElementAt(4).Text, dati.ElementAt(5).Text, dati.ElementAt(6).Text, dati.ElementAt(7).Text, dati.ElementAt(8).Text));
            }
            return l;
        }
    }
}