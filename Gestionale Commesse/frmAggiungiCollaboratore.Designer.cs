﻿namespace Gestionale_Commesse
{
    partial class frmAggiungiCollaboratore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAggiungiCollaboratore));
            this.cmbCollaborazione = new System.Windows.Forms.ComboBox();
            this.btnAggiungi = new System.Windows.Forms.Button();
            this.cmbreparto = new System.Windows.Forms.ComboBox();
            this.txtcomuneresidenza = new System.Windows.Forms.TextBox();
            this.dtpassunzione = new System.Windows.Forms.DateTimePicker();
            this.txtcellulare = new System.Windows.Forms.TextBox();
            this.txttelinterno = new System.Windows.Forms.TextBox();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.txtcognome = new System.Windows.Forms.TextBox();
            this.lblComune = new System.Windows.Forms.Label();
            this.lblData = new System.Windows.Forms.Label();
            this.lblCollaborazione = new System.Windows.Forms.Label();
            this.lblCellulare = new System.Windows.Forms.Label();
            this.lblTelInterno = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblReparto = new System.Windows.Forms.Label();
            this.lblNome = new System.Windows.Forms.Label();
            this.lblCognome = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmbCollaborazione
            // 
            this.cmbCollaborazione.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbCollaborazione.FormattingEnabled = true;
            this.cmbCollaborazione.Location = new System.Drawing.Point(377, 70);
            this.cmbCollaborazione.Name = "cmbCollaborazione";
            this.cmbCollaborazione.Size = new System.Drawing.Size(172, 21);
            this.cmbCollaborazione.Sorted = true;
            this.cmbCollaborazione.TabIndex = 15;
            // 
            // btnAggiungi
            // 
            this.btnAggiungi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAggiungi.Location = new System.Drawing.Point(264, 130);
            this.btnAggiungi.Name = "btnAggiungi";
            this.btnAggiungi.Size = new System.Drawing.Size(285, 29);
            this.btnAggiungi.TabIndex = 18;
            this.btnAggiungi.Text = "Aggiungi";
            this.btnAggiungi.UseVisualStyleBackColor = true;
            this.btnAggiungi.Click += new System.EventHandler(this.btnAggiungi_Click);
            // 
            // cmbreparto
            // 
            this.cmbreparto.FormattingEnabled = true;
            this.cmbreparto.Location = new System.Drawing.Point(123, 73);
            this.cmbreparto.Name = "cmbreparto";
            this.cmbreparto.Size = new System.Drawing.Size(121, 21);
            this.cmbreparto.Sorted = true;
            this.cmbreparto.TabIndex = 5;
            // 
            // txtcomuneresidenza
            // 
            this.txtcomuneresidenza.Location = new System.Drawing.Point(125, 137);
            this.txtcomuneresidenza.Name = "txtcomuneresidenza";
            this.txtcomuneresidenza.Size = new System.Drawing.Size(120, 20);
            this.txtcomuneresidenza.TabIndex = 9;
            // 
            // dtpassunzione
            // 
            this.dtpassunzione.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpassunzione.Location = new System.Drawing.Point(377, 99);
            this.dtpassunzione.Name = "dtpassunzione";
            this.dtpassunzione.Size = new System.Drawing.Size(172, 20);
            this.dtpassunzione.TabIndex = 17;
            // 
            // txtcellulare
            // 
            this.txtcellulare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtcellulare.Location = new System.Drawing.Point(377, 41);
            this.txtcellulare.Name = "txtcellulare";
            this.txtcellulare.Size = new System.Drawing.Size(172, 20);
            this.txtcellulare.TabIndex = 13;
            // 
            // txttelinterno
            // 
            this.txttelinterno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txttelinterno.Location = new System.Drawing.Point(377, 12);
            this.txttelinterno.Name = "txttelinterno";
            this.txttelinterno.Size = new System.Drawing.Size(172, 20);
            this.txttelinterno.TabIndex = 11;
            // 
            // txtemail
            // 
            this.txtemail.Location = new System.Drawing.Point(124, 105);
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(120, 20);
            this.txtemail.TabIndex = 7;
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(125, 41);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(120, 20);
            this.txtnome.TabIndex = 3;
            this.txtnome.TextChanged += new System.EventHandler(this.txtnome_TextChanged);
            // 
            // txtcognome
            // 
            this.txtcognome.Location = new System.Drawing.Point(125, 12);
            this.txtcognome.Name = "txtcognome";
            this.txtcognome.Size = new System.Drawing.Size(120, 20);
            this.txtcognome.TabIndex = 1;
            this.txtcognome.TextChanged += new System.EventHandler(this.txtcognome_TextChanged);
            // 
            // lblComune
            // 
            this.lblComune.AutoSize = true;
            this.lblComune.Location = new System.Drawing.Point(11, 140);
            this.lblComune.Name = "lblComune";
            this.lblComune.Size = new System.Drawing.Size(105, 13);
            this.lblComune.TabIndex = 8;
            this.lblComune.Text = "Comune di residenza";
            // 
            // lblData
            // 
            this.lblData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblData.AutoSize = true;
            this.lblData.Location = new System.Drawing.Point(261, 105);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(97, 13);
            this.lblData.TabIndex = 16;
            this.lblData.Text = "Data di assunzione";
            // 
            // lblCollaborazione
            // 
            this.lblCollaborazione.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCollaborazione.AutoSize = true;
            this.lblCollaborazione.Location = new System.Drawing.Point(261, 73);
            this.lblCollaborazione.Name = "lblCollaborazione";
            this.lblCollaborazione.Size = new System.Drawing.Size(110, 13);
            this.lblCollaborazione.TabIndex = 14;
            this.lblCollaborazione.Text = "Tipo di collaborazione";
            // 
            // lblCellulare
            // 
            this.lblCellulare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCellulare.AutoSize = true;
            this.lblCellulare.Location = new System.Drawing.Point(261, 44);
            this.lblCellulare.Name = "lblCellulare";
            this.lblCellulare.Size = new System.Drawing.Size(47, 13);
            this.lblCellulare.TabIndex = 12;
            this.lblCellulare.Text = "Cellulare";
            // 
            // lblTelInterno
            // 
            this.lblTelInterno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTelInterno.AutoSize = true;
            this.lblTelInterno.Location = new System.Drawing.Point(261, 15);
            this.lblTelInterno.Name = "lblTelInterno";
            this.lblTelInterno.Size = new System.Drawing.Size(58, 13);
            this.lblTelInterno.TabIndex = 10;
            this.lblTelInterno.Text = "Tel.Interno";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(10, 108);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(32, 13);
            this.lblEmail.TabIndex = 6;
            this.lblEmail.Text = "Email";
            // 
            // lblReparto
            // 
            this.lblReparto.AutoSize = true;
            this.lblReparto.Location = new System.Drawing.Point(10, 76);
            this.lblReparto.Name = "lblReparto";
            this.lblReparto.Size = new System.Drawing.Size(45, 13);
            this.lblReparto.TabIndex = 4;
            this.lblReparto.Text = "Reparto";
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(11, 44);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(35, 13);
            this.lblNome.TabIndex = 2;
            this.lblNome.Text = "Nome";
            // 
            // lblCognome
            // 
            this.lblCognome.AutoSize = true;
            this.lblCognome.Location = new System.Drawing.Point(12, 15);
            this.lblCognome.Name = "lblCognome";
            this.lblCognome.Size = new System.Drawing.Size(52, 13);
            this.lblCognome.TabIndex = 0;
            this.lblCognome.Text = "Cognome";
            // 
            // frmAggiungiCollaboratore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 173);
            this.Controls.Add(this.cmbCollaborazione);
            this.Controls.Add(this.btnAggiungi);
            this.Controls.Add(this.cmbreparto);
            this.Controls.Add(this.txtcomuneresidenza);
            this.Controls.Add(this.dtpassunzione);
            this.Controls.Add(this.txtcellulare);
            this.Controls.Add(this.txttelinterno);
            this.Controls.Add(this.txtemail);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.txtcognome);
            this.Controls.Add(this.lblComune);
            this.Controls.Add(this.lblData);
            this.Controls.Add(this.lblCollaborazione);
            this.Controls.Add(this.lblCellulare);
            this.Controls.Add(this.lblTelInterno);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblReparto);
            this.Controls.Add(this.lblNome);
            this.Controls.Add(this.lblCognome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmAggiungiCollaboratore";
            this.Text = "Aggiungi un collaboratore";
            this.Load += new System.EventHandler(this.frmAggiungiCollaboratore_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbCollaborazione;
        private System.Windows.Forms.Button btnAggiungi;
        private System.Windows.Forms.ComboBox cmbreparto;
        private System.Windows.Forms.TextBox txtcomuneresidenza;
        private System.Windows.Forms.DateTimePicker dtpassunzione;
        private System.Windows.Forms.TextBox txtcellulare;
        private System.Windows.Forms.TextBox txttelinterno;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.TextBox txtcognome;
        private System.Windows.Forms.Label lblComune;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.Label lblCollaborazione;
        private System.Windows.Forms.Label lblCellulare;
        private System.Windows.Forms.Label lblTelInterno;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblReparto;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label lblCognome;
    }
}