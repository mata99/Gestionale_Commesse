﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Gestionale_Commesse
{
    public partial class frmAggiungiCollaboratore : Form
    {

        public frmAggiungiCollaboratore()
        {
            InitializeComponent();
        }

        private void frmAggiungiCollaboratore_Load(object sender, EventArgs e)
        {
            try
            {
                // Carico il ComboBox con i nomi dei reparti ed abilito l'autocomplete
                cmbreparto.Items.AddRange(TCollaboratore.Reparto().ToArray());
                cmbreparto.AutoCompleteSource = AutoCompleteSource.ListItems;
                cmbreparto.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

                // Carico il ComboBox con i tipi di collaborazione ed abilito l'autocomplete
                cmbCollaborazione.Items.AddRange(TCollaboratore.TipoCollaborazione().ToArray());
                cmbCollaborazione.AutoCompleteSource = AutoCompleteSource.ListItems;
                cmbCollaborazione.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            btnAggiungi.Enabled = false;

        }

        /// <summary>
        /// Funzione che aggiunge un nuovo collaboratore alla tabella Collaboratore
        /// </summary>
        private void btnAggiungi_Click(object sender, EventArgs e)
        {
            try
            {
                List<Collaboratore> l = new List<Collaboratore>();
                l.Add(new Collaboratore(txtnome.Text, txtcognome.Text, cmbreparto.Text, txtemail.Text, txttelinterno.Text, txtcellulare.Text, cmbCollaborazione.Text, dtpassunzione.Value.ToShortDateString(), txtcomuneresidenza.Text));
                TCollaboratore.Update(l);
                Pulisci();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Metodo che pulisce il contenuto delle textbox e combobox del form
        /// </summary>
        void Pulisci()
        {
            txtcognome.Clear();
            txtnome.Clear();
            cmbreparto.Text = "";
            txtemail.Clear();
            txtcomuneresidenza.Clear();
            txttelinterno.Clear();
            txtcellulare.Clear();
            cmbCollaborazione.Text = "";
        }

        /// <summary>
        /// Abilita il pulsante btnAggiungi se è presente almeno un carattere in ogni casella di testo,
        /// altrimenti lo disabilita
        /// </summary>
        private void Abilita()
        {
            if (!string.IsNullOrEmpty(txtnome.Text) && !string.IsNullOrEmpty(txtcognome.Text))
                btnAggiungi.Enabled = true;
            else
                btnAggiungi.Enabled = false;
        }

        private void txtcognome_TextChanged(object sender, EventArgs e)
        {
            Abilita();
        }

        private void txtnome_TextChanged(object sender, EventArgs e)
        {
            Abilita();
        }
    }
}
